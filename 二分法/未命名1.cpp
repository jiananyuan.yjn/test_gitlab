#include<iostream>
#include<vector>
#include<cmath>
#include<iomanip>
double f(double x);
double Half(double left,double right);
using namespace std;
double a,b,c,d;
int main()
{
	cin>>a>>b>>c>>d;
	vector<double>v;
	for(double i=-100;i<=100;i++)
	{
		if( !f(i) )
			v.push_back(i);
		if( f(i)*f(i+1)<0 )
			v.push_back(i);
	}
	
	for(int i=0;i<v.size();i++)
	{
		if( !f(i) )
			continue;
		else
			v[i]=Half(v[i],v[i]+1);
	}
	
	for(int i=0;i<v.size();i++)
		cout<<fixed<<setprecision(2)<<v[i]<<' ';
	
	return 0;
}

double f(double x)
{
	return a*x*x*x+b*x*x+c*x+d;
}

double Half(double left,double right)
{
	if(fabs(right-left)<0.001)
		return (right+left)/2;
	
	double mid=(right+left)/2;
	
	if( f(mid)*f(left)<=0 )
		return Half(left,mid);
	else
		return Half(mid,right);
}
