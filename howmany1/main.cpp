#include <iostream>
using namespace std;
typedef long long ll;
int main()
{
    int n;
    while(cin>>n) {
        ll s = 0;
        int cnt=0;
        do {
            s = (s%n)*10+1;
            cnt++;
        }while(s%n!=0);
        cout<<cnt<<endl;
    }
    return 0;
}
