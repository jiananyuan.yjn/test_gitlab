#include <iostream>
#include <string>
#include <cctype>
using namespace std;
struct Matrix {
    char name;
    int row;
    int col;
    Matrix(int a=0,int b=0):row(a),col(b){}
};
template<class T>
//ջ��ָ���ջ����1
class MyStack {
protected:
    int MAXN;
    int ptr;
    T data[10000];
public:
    MyStack():MAXN(10000),ptr(0){}
    bool Push(T val) {
        if(ptr==MAXN)  return false;
        data[ptr]=val;
        ptr++;
        return true;
    }
    bool Pop() {
        if(ptr==0)  return false;
        ptr--;
        return true;
    }
    T Top() {
        return data[ptr-1];
    }
    bool is_Empty() {
        return ptr == 0;
    }
    bool is_Full() {
        return ptr==MAXN;
    }
    void make_Empty() {
        ptr = 0;
    }
    int Size(){return ptr;}
    ~MyStack(){}
};
int main() {
    int n;
    cin>>n;
    Matrix p[n];
    for(int i=0;i<n;i++) {
        cin>>p[i].name>>p[i].row>>p[i].col;
    }
    string s;
    while(cin>>s) {
        MyStack<Matrix> v;
        int ans=0,flag=0;
        for(int i=0;i<s.length();i++) {
            if(isalpha(s[i])) {
                for(int j=0;j<n;j++) {
                    if(p[j].name==s[i]) {
                        v.Push(p[j]);
                        break;
                    }
                }
            }
            else if(s[i]==')') {
                Matrix a = v.Top();
                v.Pop();
                Matrix b= v.Top();
                v.Pop();
                if(b.col==a.row) {
                    ans += b.row*b.col*a.col;
                    v.Push(Matrix(b.row,a.col));
                }
                else {
                    cout<<"error"<<endl;
                    flag = 1;
                    break;
                }
            }
        }
        if(flag==0)  cout<<ans<<endl;
    }
    return 0;
}
