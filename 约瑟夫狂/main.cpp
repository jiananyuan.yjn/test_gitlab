#include<iostream>
using namespace std;
struct node{
    int data;
    node *next;
}*head,*tail;

void initialize(){
    head=new node;
    head->data=0;
    head->next=0;
    tail=head;
}
void Create_Linklist(int n){
    head->data=n;
    for(int i=1;i<=n;i++){
        node *p=new node;
        p->data=i;
        p->next=0;
        tail->next=p;
        tail=p;
    }
    tail->next=head->next;
}

void Delete_Linklist(int d){
    head->data--;
    node* p=head;
    node *q=head->next;
    for(int i=1;i<d;i++){
        p=p->next;
        q=q->next;
    }
    p->next=q->next;
    cout<<q->data<<' ';
    delete q;
}

int Find_in_Linklist(int a){
    node *p=head->next;
    int k=1;
    while(k<=head->data){
        if(p->data==a)
            return k;

        else{
            k++;
            p=p->next;
        }
    }
    return 0;
}

void Destory_Linklist(){
    node *temp=head->next;
    while(head->data!=0){
        head->data--;
        node *p=temp->next;
        node *q=temp->next->next;
        temp->next=q;
        delete p;
    }
}

int main(){
    int n,k,m;
    while(cin>>n>>k>>m){
        initialize();
        Create_Linklist(n);
        int count=1,people=k;
        while(head->data>1){
            int flag=Find_in_Linklist(people);
            if(!flag)
                goto tag;
            if(count==m)
                Delete_Linklist(flag);
            count++;
            tag:people++;
            if(count==m+1)
                count=1;

            if(people==n+1)
                people=1;
        }
        cout<<head->next->data<<endl;
    }
    Destory_Linklist();
    return 0;
}
