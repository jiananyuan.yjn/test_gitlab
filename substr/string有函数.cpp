#include<bits/stdc++.h>
using namespace std;
int substr(string *s1,string *s2,int index);
int main()
{
    int n;
    cin>>n;
    cin.get();
    while(n--)
    {
        string v,s;
        getline(cin,s);
        int index;
        cin>>index;
        cin.get();
        int flag=substr(&s,&v,index);
        if(flag==1)
            cout<<v<<endl;
        else
            cout<<"IndexError"<<endl;
    }
 
    return 0;
}

int substr(string *s1,string *s2,int index)
{
    int len=(*s1).size();
    if(index>=len)
        return 0;
    for(int i=index;i<len;i++)
        *s2+=(*s1)[i];
    return 1;
}
