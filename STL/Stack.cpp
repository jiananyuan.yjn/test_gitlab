#include<iostream>
#include<stack>
using namespace std;
int main()
{
	stack<int>s;
	s.push(3);
	s.push(7);
	s.push(1);
	cout<<s.size()<<endl;//3
	cout<<s.top()<<endl;//1
	s.pop();
	cout<<s.top()<<endl;//7
	s.pop();
	cout<<s.top()<<endl;//3
	s.push(5);
	cout<<s.top()<<endl;//5
	s.pop();
	cout<<s.top()<<endl;//3
	return 0;
}
