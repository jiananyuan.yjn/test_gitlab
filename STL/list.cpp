#include<iostream>
#include<list>
using namespace std;
int main()
{
	list<char> L;
	L.push_front('b');
	L.push_back('c');
	L.push_front('a');//  abc
	cout<<L.front()<<endl;
	cout<<L.back()<<endl;
	L.pop_front();
	L.push_back('d');
	cout<<L.front()<<endl;
	cout<<L.back()<<endl;
	L.insert(L.begin(),'4');
	cout<<L.front()<<endl;
	return 0;
}
