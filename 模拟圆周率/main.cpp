#include <bits/stdc++.h>
#define RANGE 1000
#define random() ((rand())%RANGE)
typedef long long LL;
const LL r  = RANGE / 2;
const LL r2 = r * r;
const LL S  = RANGE * RANGE;
using namespace std;
int main() {
    srand((unsigned)time(NULL));
    LL ans = 0 ;
    for(int j = 1; j <=1e9; j++) {
        int x = random();
        int y = random();
        LL dis2 = (x-r)*(x-r)+(y-r)*(y-r);
        if(dis2 <= r2) {
            ans ++;
        }
    }
    double rate = ans*1.0 / 1e9;
    cout<< rate <<endl;
    double s = S * rate;
    cout << s / r2 << endl;
    return 0;
}
