#include <iostream>
using namespace std;
int apple(int m,int n)
{
    if(n==1||m==0)
        return 1;
    if(m>=n)
        return apple(m-n,n)+apple(m,n-1);
    if(m<n)
        return apple(m,m);
}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int m,n;
        cin>>m>>n;
        cout<<apple(m,n)<<endl;
    }
    return 0;
}
