//静态查找模板
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <algorithm>
#include <math.h>
#define MAX 10005
#define N 200
#define NIL 0
using namespace std;
typedef int Element;
typedef Element Bintree;


//顺序查找
int Search_Seq(Element a[],int n,Element key) {
    a[0] = key;
    int i = n;
    while(a[i] != key) i--;
    return i;
}

//二分查找
bool Search_Binary(Element a[],int n,Element key) {
    int l = 1;
    int r = n;
    Element d[MAX];
    memcpy(d,a,sizeof(int)*(n+1));
    while(l <= r) {
        int mid = (l+r)/2;
        if(d[mid] == key) return true;
        else if(d[mid] >  key) r = mid - 1;
        else if(d[mid] <  key) l = mid + 1;
    }
    return false;
}

//静态树表
Bintree T[MAX];
void Search_Tree(int pos,Element a[],float sw[],int low,int high) {
    int i = low;
    int minx = fabs(sw[high] - sw[low]);
    int dw = sw[high] + sw[low-1];
    for(int j = low+1;j <= high;j++) {
        if(fabs(dw-sw[j]-sw[j-1]) < minx) {
            i = j;
            minx = fabs(dw-sw[j]-sw[j-1]);
        }
    }
    T[pos] = a[i];
    if(i == low) T[2*i] = NIL;
    else Search_Tree(2*i,a,sw,low,i-1);
    if(i == high) T[2*i+1] = NIL;
    else Search_Tree(2*i+1,a,sw,i+1,high);
}

//分块查找
typedef struct node {
    int key;
    int stid;
} Index;

int Search_Blocks(Element a[],int n,Element x) {
    Index index[N];
    int m; cin>>m; //想分成几块？
    int keynum = n/m; //每块包含的data数
    for(int i=1;i<=m;i++) {
        index[i].key = a[i*keynum];
        for(int j = i*keynum + 1;j<=i*keynum + keynum;j++) {
            if(index[i].key < a[j]) {
                index[i].key = a[j];
                index[i].stid = j;
            }
        }
    }  //initialize
    int l = 1,r = m, mid = 0;
    while(r >= 1 && l <= r) {
        mid = (l+r)/2;
        if(index[mid].key == x) {
            r = mid;
            break;
        }
        else if(index[mid].key > x) r = mid - 1;
        else if(index[mid].key < x) l = mid + 1;
    }
    int i = 0;
    if(x > index[mid].key) i = index[mid+1].stid;
    else i = index[mid].stid;
    i = i/keynum + 1;
    int pos = -1;
    for(int j = i*keynum - keynum + 1;j <= i*keynum && j<m*keynum;j++) {
        if(x == a[j]) {pos = j;break;}
    }
    return pos;
}


int main(){
    Element a[MAX];
    int n; cin>>n;
    for(int i=1;i<=n;i++) scanf("%d",&a[i]);
    return 0;
}
