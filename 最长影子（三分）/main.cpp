#include <iostream>
#include<stdio.h>
using namespace std;
const double eps = 1e-10;
double h,H,D;
double f(double x)
{
    if(x > (D-h*D/H) )
        return h-(D-x)/x*(H-h)+(D-x);
    else
        return h/(H-h)*x;
}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        cin>>H>>h>>D;
        double r=D,l=0;
        while(r-l>eps)
        {
            double mid=(l+r)/2.0;
            double mmid=(mid+r)/2.0;
            if(f(mid)>f(mmid))
                r=mmid;
            else
                l=mid;
        }
        printf("%.3lf\n",f(l));
    }
    return 0;
}
