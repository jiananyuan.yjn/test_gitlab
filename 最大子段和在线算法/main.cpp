#include <iostream>
#define MAX 10000005
using namespace std;
int arr[MAX];
int solve(int* a,int n) {
    int s = 0;
    int rec = 0;
    for(int i=0;i<n;i++) {
        s += a[i];
        if(s >rec) rec = s;
        if(s < 0) s = 0;
    }
    return rec;
}
int main()
{
    int n; cin>>n;
    for(int i=0;i<n;i++) cin>>arr[i];
    cout<<solve(arr,n)<<endl;
    return 0;
}
