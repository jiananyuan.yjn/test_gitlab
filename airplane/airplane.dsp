# Microsoft Developer Studio Project File - Name="airplane" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=airplane - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "airplane.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "airplane.mak" CFG="airplane - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "airplane - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "airplane - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "airplane - Win32 Release"

# PROP BASE Use_MFC
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f airplane.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "airplane.exe"
# PROP BASE Bsc_Name "airplane.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "NMAKE /f airplane.mak"
# PROP Rebuild_Opt "/a"
# PROP Target_File "airplane.exe"
# PROP Bsc_Name "airplane.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "airplane - Win32 Debug"

# PROP BASE Use_MFC
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f airplane.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "airplane.exe"
# PROP BASE Bsc_Name "airplane.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "nmake /f "airplane.mak""
# PROP Rebuild_Opt "/a"
# PROP Target_File "airplane.exe"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "airplane - Win32 Release"
# Name "airplane - Win32 Debug"

!IF  "$(CFG)" == "airplane - Win32 Release"

!ELSEIF  "$(CFG)" == "airplane - Win32 Debug"

!ENDIF 

# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\c1.cpp
# End Source File
# Begin Source File

SOURCE=.\c2.cpp
# End Source File
# Begin Source File

SOURCE=.\c3.cpp
# End Source File
# Begin Source File

SOURCE=.\c4.cpp
# End Source File
# Begin Source File

SOURCE=.\c5.cpp
# End Source File
# Begin Source File

SOURCE=.\c6.cpp
# End Source File
# Begin Source File

SOURCE=.\c7.cpp
# End Source File
# Begin Source File

SOURCE=.\c8.cpp
# End Source File
# Begin Source File

SOURCE=.\c9.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\h1.h
# End Source File
# Begin Source File

SOURCE=.\h2.h
# End Source File
# Begin Source File

SOURCE=.\h3.h
# End Source File
# Begin Source File

SOURCE=.\h4.h
# End Source File
# Begin Source File

SOURCE=.\h5.h
# End Source File
# Begin Source File

SOURCE=.\h6.h
# End Source File
# Begin Source File

SOURCE=.\h7.h
# End Source File
# Begin Source File

SOURCE=.\h8.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=.\c4.asp
# End Source File
# Begin Source File

SOURCE=.\h7.asp
# End Source File
# End Target
# End Project
