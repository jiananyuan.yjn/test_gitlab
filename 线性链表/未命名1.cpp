#include<iostream>
#include<cstdlib>
typedef struct Grade_Info{
            int score;
            struct Grade_Info *next;
            } NODE;

NODE *Create_LinkList();
void Insert_LinkList (NODE *head,NODE *pnew,int i);
void Delete_LinkList (NODE *head,int i);
void Display_LinkList (NODE *head);
void Free_LinkList (NODE *head);
using namespace std;
int main()
{
    NODE *head,*pnew;
    head=Create_LinkList();
    if(head==NULL)
        return 0;
    cout<<"after create:";
    Display_LinkList (head);

    pnew=(NODE *)malloc (sizeof(NODE));
    if(pnew==NULL)
    {
        cout<<"no enough memory"<<endl;
        return 0;
    }
    pnew->score=88;
    Insert_LinkList(head,pnew,3);
    cout<<"after insert:";
    Display_LinkList (head);

    Delete_LinkList(head,3);
    cout<<"after delete:";
    Display_LinkList(head);

    Free_LinkList(head);
    return 0;
}

NODE *Create_LinkList()
{
    NODE *head,*tail,*pnew;
    int score;
    head=(NODE *)malloc(sizeof(NODE));
    if(head==NULL)
    {
        cout<<"no enough memory!"<<endl;
        return (NULL);
    }
    head->next=NULL;
    tail=head;

    cout<<"input the score of students:"<<endl;
    while(1)
    {
        cin>>score;
        if(score<0)
            break;
        pnew=(NODE *)malloc(sizeof(NODE));
        if(pnew==NULL)
        {
            cout<<"no enough money!"<<endl;
            return (NULL);
        }
        pnew->score=score;
        pnew->next=NULL;

        tail->next=pnew;
        tail=pnew;
    }
    return (head);
}

void Insert_LinkList (NODE *head,NODE *pnew,int i)
{
    NODE *p;
    int j;
    p=head;
    for(j=0;j<i && p!=NULL;j++)
        p=p->next;
    if(p==NULL)
    {
        cout<<"the "<<i<<" node not found"<<endl;
        return ;
    }
    pnew->next=p->next;
    p->next=pnew;
}

void Delete_LinkList(NODE *head,int i)
{
    NODE *p,*q;
    int j;

    if(i==0)
        return ;

    p=head;
    for(j=1;j<i && p->next !=NULL;j++)
        p=p->next;
    if(p->next==NULL)
    {
        cout<<"the "<<i<<" node not found!"<<endl;
        return ;
    }

    q=p->next;
    p->next=q->next;
    free(q);
}

void Display_LinkList(NODE *head)
{
    NODE *p;
    for(p=head->next;p!=NULL;p=p->next)
        cout<<p->score<<' ';
    cout<<endl;
}

void Free_LinkList(NODE *head)
{
    NODE *p,*q;
    p=head;
    while(p->next!=NULL)
    {
        q=p->next;
        p->next=q->next;
        free(q);
    }
    free(head);
}
