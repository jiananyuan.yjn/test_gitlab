#include<bits/stdc++.h>
#define MAX 16777218
using namespace std;
typedef long long ll;
inline int read(){
    int X=0;bool d=0;char ch=0;
    while(!isdigit(ch)) d|=ch=='-',ch=getchar();
    while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
    return d?-X:X;
}
int a[MAX];
ll n;
ll ans = -1e9;
ll A(int c) {
    ll s = 1;
    while(c--) s = s << 1;
    return (ll)(s-1);
}
void build(ll pos) {
    if(pos<=n) {
        a[pos] = read();
        build(pos<<1);
        build((pos<<1)|1);
    }
}
void dfs(ll pos,ll sum) {
    if(pos<=n) {
        dfs(pos<<1,sum+a[pos]);
        ans = max(ans,sum+a[pos]);
        dfs((pos<<1)|1,sum+a[pos]);
        ans = max(ans,sum+a[pos]);
    }
}
int main() {
    cin>>n;
    n = A(n);
    build(1);
    dfs(1,0);
    cout<<ans<<endl;
    return 0;
}
