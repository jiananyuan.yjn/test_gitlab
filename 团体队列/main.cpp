#include <iostream>
#include<queue>
#include<map>
using namespace std;
#define MAX 1001
int main(){
    int t;
    int m=1;
    while(cin>>t && t){
        map<int,int> v;//建立队员到某队的映射
        for(int i=1;i<=t;i++){
            int n;
            cin>>n;
            while(n--){
                int x;
                cin>>x;
                v[x]=i;
            }
        }
        cout<<"Scenario #"<<m++<<endl;
        queue<int> team[MAX];
        queue<int> total;
        string op;
        while(cin>>op){
            if(op=="STOP")  break;
            if(op=="ENQUEUE"){
                int x;
                cin>>x;
                int t=v[x];
                if(team[t].empty())     total.push(t);
                team[t].push(x);
            }
            if(op=="DEQUEUE"){
                if(!total.empty()){
                    int t=total.front();
                    cout<<team[t].front()<<endl;
                    team[t].pop();
                    if(team[t].empty())     total.pop();
                }
            }
        }
        cout<<endl;
    }
    return 0;
}
