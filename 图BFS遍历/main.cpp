#include <bits/stdc++.h>
using namespace std;
int n,m,a[20][20],book[100];
queue<int> v;
void bfs() {
    book[1]=1;
    v.push(1);
    while(!v.empty()&&v.size()<=n) {
        int cur=v.front();
        for(int i=1;i<=n;i++) {
            if(a[cur][i]==1 && book[i]==0 ) {
                v.push(i);
                book[i]=1;
            }
        }
        cout<<v.front()<<" ";
        v.pop();
    }
}
int main()
{
    cin>>n>>m;
    while(m--) {
        int c,r;
        cin>>c>>r;
        a[c][r]=a[r][c]=1;
    }
    bfs();
    return 0;
}
