#include <iostream>
#include<cstring>
using namespace std;
int n,mod=1e9+7,k;
typedef long long LL;
#define MAX 120
struct Mat{
    LL a[MAX][MAX];
    Mat()
    {
        for(int i=1;i<=MAX;i++)
            for(int j=1;j<=MAX;j++)
            {
                if(i==j)
                    a[i][i]=1;
                else
                    a[i][j]=0;
            }
    }
};
Mat mul(Mat& p,Mat& q)
{
    Mat ans;
    memset(ans.a,0,sizeof(ans.a));
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
        {
            for(int k=1;k<=n;k++)
            {
                ans.a[i][j]=(ans.a[i][j]%mod+p.a[i][k]*q.a[k][j]%mod)%mod;
            }
        }
    }
    return ans;
}
Mat add(Mat& p,Mat& q)
{
    Mat ans;
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
        {
            ans.a[i][j]=(p.a[i][j]+q.a[i][j])%mod;
        }
    }
    return ans;
}
Mat qpow(Mat &m,int p)
{
    if(p&1) return m;
    Mat tmp;
    if(p&1)
        tmp=m;
    Mat ans=qpow(m,p/2);
    ans=mul(ans,ans);
    if(p&1)
        ans=mul(ans,tmp);
    return ans;
}
int main()
{
    Mat test;
    cin>>n>>k;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            cin>>test.a[i][j];
    Mat ans;
    memset(ans.a,0,sizeof(ans.a));
    for(int i=1;i<=k;i++)
    {
        Mat temp=qpow(test,i);
        ans=add(ans,temp);
    }
    for(int i=1;i<=n;i++)
    {
        cout<<ans.a[i][1];
        for(int j=2;j<=n;j++)
            cout<<" "<<ans.a[i][j];
        cout<<endl;
    }
    return 0;
}
