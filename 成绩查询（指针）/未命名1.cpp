#include<iostream>
using namespace std;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		int *p=new int[n];
		for(int i=0;i<n;i++)
			cin>>p[i];
		int x;
		cin>>x;
		int *q=p+n/2;
		cout<<*(q-1)<<' '<<*(q+1)<<endl;
		if(x<=n/2+1)
			cout<<*(q-(n/2+1-x))<<endl;
		else
			cout<<*(q+(x-n/2-1))<<endl;
		delete []p;
	}
	return 0;
}
