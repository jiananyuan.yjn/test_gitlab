#include<iostream>
#include<algorithm>
using namespace std;
typedef struct Student{
	int num;
	int height;
}Student;
bool cmp(Student x,Student y)
{
	return x.height>y.height;
}
int main()
{
	int n;
	cin>>n;
	Student *stu=new Student[n+1];
	for(int i=0;i<n;i++)
	{
		cin>>stu[i].height;
		stu[i].num=i+1;
	}
	
	sort(stu,stu+n,cmp);
	
	for(int i=0;i<n;i++)
		cout<<stu[i].num<<' ';
}
