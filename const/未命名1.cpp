#include<iostream>
#include<string>
using namespace std;
class CStudent{
private:
	string name;
	string sex;
	long num;
	string college;
	long what;
public:
	CStudent(){}
	void set(string a,string b,long c,string d,long f){name=a;sex=b;num=c;college=d;what=f;}
	string get_Name() const {return name;}
	CStudent(const CStudent& ss){name=ss.name;sex=ss.sex;num=ss.num;college=ss.college;what=ss.what;}
	~CStudent(){}
	void print(){cout<<name<<endl;}
};
bool cmp(const CStudent& a,const CStudent& b);
int main()
{
	int n;
	cin>>n;
	cin.ignore();
	CStudent *s=new CStudent[n];
	for(int i=0;i<n;i++)
	{
		string a,b,c;
		long d,f;
		cin>>a;
		cin>>b;
		cin>>c;
		cin>>d;
		cin>>f;
		cin.ignore();
		s[i].set(a,b,d,c,f);
	}
	
	for(int i=0;i<n-1;i++)
	{
		int k=i;
		for(int j=i+1;j<n;j++)
		{
			bool tag=cmp( s[j],s[k] );
			if(tag)
				k=j;
		}
		
		if(k!=i)
		{
			CStudent temp(s[i]);
			s[i]=s[k];
			s[k]=temp;
		}
	}
	
	for(int i=0;i<n;i++)
		s[i].print();
	
	delete[]s;
	return 0;
}

bool cmp(const CStudent& a,const CStudent& b)
{
	string temp1=a.get_Name();
	string temp2=b.get_Name();
	int flag=temp1.compare(temp2);
	if(flag>0)
		return true;
	return false;
}
