#include <iostream>
#include <string>
using namespace std;
int share(string a,string b)//a是短的
{
    int maxn=0;
    for(int i=0;i<a.length()/2+1;i++)
    {//枚举起点
        for(int j=a.length();j>=1;j--)
        {
            string tmp=a.substr(i,j);
            if(b.find(tmp)!=string::npos)
                maxn=maxn>tmp.length()?maxn:tmp.length();
        }
    }
    return maxn;
}
int main()
{
    string a,b;
    while(cin>>a>>b)
    {
        if(a.length()==1||b.length()==1)
            return 0;
        a+=a.substr(0,a.length()-1);
        b+=b.substr(0,b.length()-1);
        if(a.length()<=b.length())
            cout<<share(a,b)<<endl;
        else
            cout<<share(b,a)<<endl;
    }
}
