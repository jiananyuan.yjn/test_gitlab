#include <iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
struct INT{
    vector<int> v;
    INT(string s="")
    {
        for(int i=0;i<s.length();i++)
        {
            int x=s[i]-'0';
            v.push_back(x);
        }
    }
    friend ostream& operator<<(ostream& out,const INT& t);
    INT operator+(INT& t)
    {
        INT rec;
        int carry=0,i,j,tmp;
        for(i=v.size()-1,j=t.v.size()-1;i>=0&&j>=0;i--,j--)
        {
            tmp=v[i]+t.v[j]+carry;
            if(tmp>=10)         carry=1;
            else if(tmp<10)     carry=0;
            rec.v.push_back(tmp%10);
        }
        for(int k=i;k>=0;k--)
        {
            tmp=v[k]+carry;
            if(tmp>=10)         carry=1;
            else if(tmp<10)     carry=0;
            rec.v.push_back(tmp%10);
        }
        for(int k=j;k>=0;k--)
        {
            tmp=t.v[k]+carry;
            if(tmp>=10)         carry=1;
            else if(tmp<10)     carry=0;
            rec.v.push_back(tmp%10);
        }
        if(tmp>=10)
        {
            rec.v.pop_back();
            rec.v.push_back(tmp);
        }
        reverse(rec.v.begin(),rec.v.end());
        return rec;
    }

};
ostream& operator<<(ostream& out,const INT& t)
{
    for(int i=0;i<t.v.size();i++)
        out<<t.v[i];
    return out;
}
int main()
{
    string a,b;
    cin>>a>>b;
    INT A=a,B=b;
    INT ans=A+B;
    cout<<ans<<endl;
    return 0;
}
