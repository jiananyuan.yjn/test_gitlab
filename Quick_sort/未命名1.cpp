#include<iostream>
#include<algorithm>
int a[100000];
using namespace std;
int main()
{
	void Quick_sort(int left,int right);
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>a[i];
	Quick_sort(0,n-1);
	for(int i=0;i<n;i++)
		cout<<a[i]<<' ';
}

void Quick_sort(int left,int right)
{
	int mid=a[left],i=left,j=right;
	if(left>right)
		return ;
	while(i!=j)
	{
		while(j>i && a[j]>=mid)
			j--; 
		while(j>i && a[i]<=mid)
			i++;
		swap(a[i],a[j]);
	}
	swap(a[left],a[i]);
	Quick_sort(left,i-1);
	Quick_sort(i+1,right);
	return ;
}
