#include <iostream>
#include <queue>
#include <vector>
#include <cstdio>
#include <cstring>
#define MAXN 21
using namespace std;
string name;
int x0,y0,x1,y1,dir,x2,y2;
const char* dirs="NESW"; //按北、东、南、西方向枚举当前朝向
const char* turns="FLR"; //按向前、左转、右转枚举转身方向
const int direction[4][2]={ {-1,0},{0,1},{1,0},{0,-1} }; //指代北、东、南、西的方向向量，注意一定要与dirs顺序一致
int has_edge[MAXN][MAXN][MAXN][MAXN]; //标记数组，标记该点处可走的路：前两个数指代当前坐标，第三个数指代当前朝向，第四个数指代转向
struct Node {
    int x,y,dir;
    Node(int xx=0,int yy=0,int ddir=0): x(xx),y(yy),dir(ddir){}
} p[MAXN][MAXN][MAXN]; //
int book[MAXN][MAXN][MAXN]; //标记数组，用来标记前两个数所组成的坐标所朝向第三个数的方向是否已走
int dir_id(char c) {
    return (strchr(dirs,c)-dirs); //符号化：根据输入的字符转换为数，例如朝北符号化为0，以此类推
}
int turn_id(char c) {
    return (strchr(turns,c)-turns); //符号化：根据输入的字符转换为数，例如左转符号化为1，以此类推
}
void read_edge() {
    memset(has_edge,0,sizeof(has_edge)); //清空
    int tx,ty,pdir,pturn;
    string tmp;
    while(cin>>tx && tx) {
        cin>>ty;
        while(cin>>tmp && tmp!="*") {
            pdir=dir_id(tmp[0]); //符号化当前朝向
            for(int i=1;i<tmp.length();i++) {
                pturn=turn_id(tmp[i]); //符号化当前转向
                has_edge[tx][ty][pdir][pturn]=1; //存在路，可走，标记为1
            }
        }
    }
}
bool read_list() {
    cin>>name;
    if(name=="END") return false;
    char dir0; //dir0为开始朝向
    cin>>x0>>y0>>dir0>>x2>>y2; //由题知：代表最初站在(x0,y0)，面向dir0方向，欲到达(x2,y2)终点处
    dir=dir_id(dir0); //dir是dir0符号化后的结果
    x1=x0+direction[dir][0]; //根据向量移动
    y1=y0+direction[dir][1];
    read_edge(); //读取接下来的输入
    return true;
}
Node walk(const Node& u,int turn) {
    dir=u.dir;
    if(turn==1) dir=(dir+3)%4; //若欲左转，比如南(2)变成东(1)
    if(turn==2) dir=(dir+1)%4; //右转
    //前进的话dir不需要做任何改变
    return Node(u.x+direction[dir][0],u.y+direction[dir][1],dir);
}
bool inside(int x,int y) {
    if(x<1 || y<1 || x>9 || y>9)    return false;
    return true;
}
void print_ans(Node u) {
    vector<Node> v; //储存结点答案的vector
    while(1) {
        v.push_back(u);
        if(book[u.x][u.y][u.dir]==0)   break; //如果只有一个方向可走了，说明已经到了起点
        u=p[u.x][u.y][u.dir]; //迭代读取与u相连的结点
    }
    v.push_back(Node(x0,y0,dir));
    cout<<name<<endl;
    int cnt=0;
    for(int i=v.size()-1;i>=0;i--) {
        if(cnt%10==0)   cout<<" ";
        printf(" (%d,%d)",v[i].x,v[i].y);
        if(++cnt%10==0) cout<<endl;
    }
    if(v.size()%10!=0)  cout<<endl;
}
void solve() { //BFS求最短路
    queue<Node> q;
    memset(book,-1,sizeof(book)); //清空
    Node n1(x1,y1,dir); //从起点走一步之后所到结点
    q.push(n1);
    book[x1][y1][dir]=0; //该结点当前朝向标记为已走，已走为0
    while(!q.empty()) {
        Node u=q.front();
        q.pop();
        if(u.x==x2 && u.y==y2) {
            print_ans(u); //如果到达终点了，输出答案
            return ;
        }
        for(int i=0;i<3;i++) { //遍历三个方向
            Node v=walk(u,i); //尝试向前走,可选择三个方向转向，逐一尝试
            if(has_edge[u.x][u.y][u.dir][i] && inside(v.x,v.y)
            && book[v.x][v.y][v.dir] < 0) { //如果这个结点“可走”，在“地图”里，且还没走过
                book[v.x][v.y][v.dir]=book[u.x][u.y][u.dir]+1; //v结点是u结点走过的第几个结点
                p[v.x][v.y][v.dir]=u; //将当前结点存放到p数组里面
                q.push(v);
            }
        }
    }
    cout<<name<<endl;
    cout<<"  No Solution Possible"<<endl;
}
int main() {
    while(read_list() ){
        solve();
    }
    return 0;
}
