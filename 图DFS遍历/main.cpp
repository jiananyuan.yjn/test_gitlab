#include <iostream>
using namespace std;
int e[100][100],book[100],sum,n;
void dfs(int cur) {
    cout<<cur<<" ";
    sum++;
    if(sum==n)  return ;
    for(int i=1;i<=n;i++) {
        if(e[cur][i]==1 && book[i]==0 ) {
            book[i]=1;
            dfs(i);
        }
    }
}
int main()
{
    cin>>n;
    int t;
    cin>>t;
    while(t--) {
        int a,b;
        cin>>a>>b;
        e[a][b]=e[b][a]=1;
    }
    book[1]=1;
    dfs(1);
    return 0;
}
