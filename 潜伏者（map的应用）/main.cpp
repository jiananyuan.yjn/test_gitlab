#include <iostream>
#include<string>
#include<map>
using namespace std;
int main()
{
    string base_info;
    cin>>base_info;
    string base_key;
    cin>>base_key;
    string des;
    cin>>des;
    map<char,char> v;
    for(char i='A';i<='Z';i++)
        v[i]='\0';
    if(base_info==base_key)
    {
        cout<<"Failed"<<endl;
        goto END;
    }
    for(int i=0;i<base_info.length();i++)
    {
        if(v[ base_info[i] ]!='\0')
        {
            if(v[ base_info[i] ]!=base_key[i])
            {
                cout<<"Failed"<<endl;
                goto END;
            }
        }
        for(map<char,char>::iterator p=v.begin();p!=v.end();p++)
        {
            if(p->second==base_key[i])
            {
                if(p->first==base_info[i])
                    break;
                else
                {
                    cout<<"Failed"<<endl;
                    goto END;
                }
            }
        }
        v[ base_info[i] ]=base_key[i];
    }
    for(int i=0;i<des.length();i++)
    {
        if(v[ des[i] ]!='\0')
            cout<<v[ des[i] ];
        else
        {
            cout<<"Failed"<<endl;
            return 0;
        }
    }
    cout<<endl;
END:return 0;
}
