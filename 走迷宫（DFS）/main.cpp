#include <bits/stdc++.h>
using namespace std;
int a[17][17],book[17][17],ans[400];
int next[4][2]={{0,-1},{-1,0},{0,1},{1,0}};
int n,m,p,q,u,tag;
void dfs(int x,int y) {
    if(x==p&&y==q) {
        cout<<"("<<ans[0]<<","<<ans[1]<<")";
        for(int i=1;i<u/2;i++)
            cout<<"->("<<ans[2*i]<<","<<ans[2*i+1]<<")";
        cout<<endl;
        tag=1;
        return ;
    }
    for(int k=0;k<4;k++) {
        int tx=x+next[k][0];
        int ty=y+next[k][1];
        if(tx<1||ty<1||tx>n||ty>m)
            continue;
        if(book[tx][ty]==0&&a[tx][ty]==1) {
            book[tx][ty]=1;
            ans[u++]=tx;
            ans[u++]=ty;
            dfs(tx,ty);
            u-=2;
            book[tx][ty]=0;
        }
    }
}
int main() {
    cin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            cin>>a[i][j];
    int x,y;
    cin>>x>>y;
    ans[u++]=x;
    ans[u++]=y;
    book[x][y]=1;
    cin>>p>>q;
    dfs(x,y);
    if(!tag)    cout<<-1<<endl;
    return 0;
}
