#include <iostream>
#include <algorithm>
using namespace std;
struct CRect {
    int w,h;
};
int main() {
    int n;
    while(cin>>n) {
        CRect a[n];
        for(int i=0;i<n;i++) cin>>a[i].w>>a[i].h;
        int pass_h = max(a[0].w,a[0].h);
        int flag=0;
        for(int i=1;i<n;i++) {
            if(min(a[i].w,a[i].h)>pass_h) {
                flag=1;
                break;
            }
            else if(a[i].w<=pass_h&&a[i].h<=pass_h)
                pass_h = max(a[i].w,a[i].h);
            else if(a[i].w<=pass_h) pass_h=a[i].w;
            else if(a[i].h<=pass_h) pass_h=a[i].h;
        }
        if(flag==1) cout<<"NO"<<endl;
        else        cout<<"YES"<<endl;
    }
    return 0;
}
