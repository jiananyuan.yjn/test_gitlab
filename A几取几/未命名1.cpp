#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
	enum color{red,yellow,blue,white,black}pri;
	int i,j,k,n=0,box;
	for(i=red;i<=black;i++)
		for(j=i+1;j<=black;j++)
			if(i!=j)
			{
				for(k=j+1;k<=black;k++)
					if(k!=i&&k!=j)
					{
						n++;
						cout<<setw(3)<<n;
						
						for(box=1;box<=3;box++)
						{
							switch(box)
							{
								case 1:pri=color(i);break;
								case 2:pri=color(j);break;
								case 3:pri=color(k);break;
							}
							
							switch(pri)
							{
								case red:   cout<<setw(8)<<"red";   break;
								case yellow:cout<<setw(8)<<"yellow";break;
								case blue:  cout<<setw(8)<<"blue";  break;
								case white: cout<<setw(8)<<"white"; break;
								case black: cout<<setw(8)<<"black"; break;
							}
						}
						cout<<endl;
					}
			}
			
	cout<<"total:"<<n<<endl;
	return 0;
} 
