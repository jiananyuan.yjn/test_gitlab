#include <iostream>
#include<string>
#include<stdio.h>
using namespace std;
struct node{
    char data;
    node *next;
}*head,*tail;
void init()
{
    head=new node;
    head->next=NULL;
    tail=head;
}
void pushback(char x)
{
    node *p=new node;
    p->data=x;
    tail->next=p;
    tail=p;
}
void display()
{
    node *p=head->next;
    while(p!=NULL)
    {
        cout<<p->data;
        p=p->next;
    }
    cout<<endl;
}
void insert(char x,int pos)
{
    node *p=new node;
    p->data=x;
    node *u=head;
    node *q=head->next;
    pos--;
    while(pos>0)
    {
        u=u->next;
        q=q->next;
        pos--;
    }
    p->next=u->next;
    u->next=p;
}
void clear()
{
    node *p;
    node *q=head->next;
    while(q!=NULL)
    {
        p=q->next;
        head->next=p;
        delete q;
        q=p;
    }
    delete head;
}
int main()
{
    string s;
    while(cin>>s)
    {
        init();
        int flag=0;
        int pos=1;
        for(unsigned int i=0;i<s.length();i++)
        {
            if(s[i]=='[')
                flag=1;
            else if(s[i]==']')
            {
                flag=0;
                pos=1;
            }
            else
            {
                if(!flag)//文本末输入
                {
                    pushback(s[i]);
                }
                else//文本头输入
                {
                    insert(s[i],pos++);
                }
            }
        }
        display();
        clear();
    }
    return 0;
}
