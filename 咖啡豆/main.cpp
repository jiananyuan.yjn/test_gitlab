#include <iostream>
#define MAX 100010
using namespace std;
int sum[MAX],a[MAX];

int main(){
    int n,k,ans=-1e9;
    cin>>n>>k;
    for(int i=1;i<=n;i++){
        cin>>a[i];
        sum[i]=sum[i-1]+a[i];
    }
    for(int i=k;i<=n;i++)
        ans=max(ans,sum[i]-sum[i-k]);
    cout<<ans<<endl;
    return 0;
}
