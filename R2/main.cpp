#include<iostream>
#include<algorithm>
using namespace std;
int main()
{
    int N,Q,kase=1;
    loop:while(cin>>N>>Q)
    {
        if(N==0 && Q==0)
            break;

        int *marble=(int *)calloc(N,sizeof(int)),i;
        for(i=0;i<N;i++)
            cin>>*(marble+i);
        sort(marble,marble+N);

        cout<<"CASE# "<<kase++<<':'<<endl;
        while(Q--)
        {
            int x;
            cin>>x;
            for(i=0;i<N;i++)
                if(*(marble+i)==x)
                {
                    cout<<x<<" found at "<<i+1<<endl;
                    goto loop;
                }

            cout<<x<<" not found"<<endl;
        }
    }
}
