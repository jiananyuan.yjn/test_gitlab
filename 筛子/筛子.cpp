#include<bits/stdc++.h>
float GG(float a[]);
using namespace std;
int main()
{
    int t;
    cin>>t;
    for(int j=1;j<=t;j++)
    {
        int i,n;
        float s,S=0;
        cin>>n;
         
        float a[7];
         
        for(i=1;i<=6;i++)
            cin>>a[i];
         
        while(n--)
        {   
            s=GG(a);
            S+=s;
        }
         
        printf("Case #%d: ",j);
        cout<<fixed<<setprecision(4)<<S<<endl;
    }
}
 
float GG(float a[])
{
    float S=0;
    for(int i=1;i<=6;i++)
        S+=i*a[i];
         
    return S;
}
