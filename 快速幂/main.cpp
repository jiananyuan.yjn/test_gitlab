#include <iostream>
using namespace std;
int M;
long long fast(long long a,long long b)
{
    long long ans=1;
    long long tmp=a;
    while(b)
    {
        if(b%2)
            ans*=tmp,ans%=M;
        tmp*=tmp,tmp%=M;
        b/=2;
    }
    return ans;
}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        cin>>M;
        int n;
        cin>>n;
        long long sum=0;
        while(n--)
        {
            long long a,b;
            cin>>a>>b;
            sum+=fast(a,b);
            sum%=M;
        }
        cout<<sum<<endl;
    }
    return 0;
}
