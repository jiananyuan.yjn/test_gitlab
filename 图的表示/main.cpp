#include <iostream>
using namespace std;
int T[105][105];
int main()
{
    int t;
    cin>>t;
    int m=t;
    while(t--)
    {
        int ori;
        cin>>ori;
        int n;
        cin>>n;
        while(n--)
        {
            int x;
            cin>>x;
            T[ori][x]=1;
        }
    }
    for(int i=1;i<=m;i++)
    {
        cout<<T[i][1];
        for(int j=2;j<=m;j++)
            cout<<" "<<T[i][j];
        cout<<endl;
    }
    return 0;
}
