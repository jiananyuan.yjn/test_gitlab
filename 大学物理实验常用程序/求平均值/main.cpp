#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    int n;
    cout<<"输入统计数：";
    while(cin>>n)
    {
        double *p=new double [n];
        double s=0;
        for(int i=0;i<n;i++)
        {
            cout<<'('<<i+1<<')'<<' ';
            cin>>p[i];
            s+=p[i];
        }
        cout<<fixed<<setprecision(6)<<"均值为："<<s/n<<endl<<endl;
        delete[]p;
        cout<<"输入统计数：";
    }
    return 0;
}
