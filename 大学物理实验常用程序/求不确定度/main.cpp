#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
int main()
{
    int n;
    cout<<"输入数据数：";
    while(cin>>n)
    {
        double *p=new double [n];
        double aver=0;
        for(int i=0;i<n;i++)
        {
            cout<<'('<<i+1<<')'<<' ';
            cin>>p[i];
            aver+=p[i];
        }
        aver/=n;
        cout<<"数据平均值为:"<<fixed<<setprecision(3)<<aver<<endl;
        double A=0;
        for(int i=0;i<n;i++)
            A+=pow( (p[i]-aver) , 2 );

        A=sqrt(A/n/(n-1));
        cout<<"A类不确定度为："<<fixed<<setprecision(3)<<A<<endl;
        cout<<"输入仪器误差：";
        double yc;
        cin>>yc;
        double B=yc/sqrt(3);
        cout<<fixed<<setprecision(3)<<"B类不确定度为："<<B<<endl;

        double hc=sqrt(pow(A,2)+pow(B,2));
        cout<<"合成不确定度为："<<fixed<<setprecision(3)<<hc<<endl<<endl;

        delete []p;
        cout<<"输入数据数：";
    }
    return 0;
}
