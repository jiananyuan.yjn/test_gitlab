#include <iostream>
#include <queue>
#include <stdio.h>
#include <algorithm>
#define MAX 12
using namespace std;
struct node {
    int ave,lst;
};
int win[MAX],num[MAX];
int main()
{
    int N; cin>>N;
    queue<node> q;
    for(int i=0;i<N;i++) {
        node tmp;
        cin>>tmp.ave>>tmp.lst;
        if(tmp.lst>60) tmp.lst = 60;
        q.push(tmp);
    }
    int K; cin>>K;
    int maxn = -0x3f3f33f,m=0,s=0;
    while(!q.empty()) {
        node cur = q.front();
        int flag=0, wait = 0x3f3f3f3f;
        for(int i=0;i<K;i++) {
            if(win[i]<=cur.ave) {
                win[i] = cur.ave + cur.lst;
                flag=1;
                num[i]++;
                break;
            }
            else {
                if(wait > win[i]-cur.ave) {
                    wait = win[i]-cur.ave;
                    m = i;
                }
            }
        }

        if(flag==0) {
            win[m] += cur.lst;
            num[m]++;
            maxn = max(maxn,wait);
            s += wait;
        }
        q.pop();
    }
    int lastest = win[0];
    for(int i=1;i<K;i++) {
        lastest = max(lastest,win[i]);
    }
    printf("%.1f %d %d\n",s*1.0/N,maxn,lastest);
    printf("%d",num[0]);
    for(int i=1;i<K;i++) {
        printf(" %d",num[i]);
    }
    return 0;
}
