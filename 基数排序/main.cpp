#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

template<class T>
class MyQueue {
protected:
    T data[10000];
    int head;
    int tail;
    int MAXN;
public:
    MyQueue (): head(0),tail(0),MAXN(10000){}
    void Push(T val) {
        data[tail]=val;
        tail++;
    }
    bool Pop() {
        if(head==tail)  return false;
        head++;
        return true;
    }
    T Top() {
        if(head==tail)  return false;
        return data[head];
    }
    bool is_Empty() {
        return head==tail;
    }
    void make_Empty() {
        head=tail=0;
    }
    int Size() {return tail-head;}
    ~MyQueue(){}
};

int maxBits(int a[],int n) {
    int maxn=0;
    for(int i=0;i<n;i++) {
        int tmp=a[i];
        int cnt=0;
        while(tmp) {
            tmp/=10;cnt++;
        }
        maxn = max(maxn,cnt);
    }
    return maxn;
}

void BaseSort(int a[],int n) {
    int m=maxBits(a,n);
    int b[n];
    memcpy(b,a,sizeof(int)*n);
    MyQueue<int> v[10];
    int base=10;
    while(m--) {
        for(int i=0;i<n;i++) {
            v[ b[i]%10 ].Push(a[i]);
        }
        int u=0;
        for(int i=0;i<10;i++) {
            while(!v[i].is_Empty()) {
                a[u++]=v[i].Top();
                v[i].Pop();
            }
        }
        for(int i=0;i<n;i++) {
            b[i] = a[i]/base;
        }
        base = base*10;
    }
}

int main() {
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++) cin>>a[i];
    BaseSort(a,n);
    for(int i=0;i<n;i++) {
        cout<<a[i]<<" ";
    }
    return 0;
}
