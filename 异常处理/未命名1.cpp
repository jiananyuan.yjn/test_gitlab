#include<iostream>
#include<cstring>
using namespace std;
class String{
public:
	String(char* str,int si)
	{
		if(si<0||si>max)
			throw Size();
			
		p=new char[si];
		strcpy(p,str);
		len=si;
	}
	class Range{
	public:
		Range(int j):index(j){}
		int index;
	};
	class Size{};
	char& operator[](int k)
	{
		if(k>=0&&k<len)
			return p[k];
		throw Range(k);
	}
private:
	char *p;
	int len;
	static int max;	
};
int String::max=20;
void g(String& str)
{
	int num=10;
	for(int i=0;i<num;i++)
		cout<<str[i];
	cout<<endl;
}
void f()
{
	try
	{
		String s("jshdkajsdhasjhdakjhdajshd",100);
		g(s);
	}
	catch(String::Range r)
	{
		cerr<<"->out of range: "<<r.index<<endl;
	}
	catch(String::Size)
	{
		cerr<<"size illegal!"<<endl;
	}
	cout<<"the program will be continued here"<<endl<<endl;
}
int main()
{
	f();
	cout<<"the code is not effected by probably expection in f()"<<endl;
	return 0;
}
