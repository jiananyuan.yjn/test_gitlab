#include <iostream>
#include<algorithm>
using namespace std;
struct tv{
    int s;
    int e;
};
bool cmp(const tv& a,const tv& b){return a.e<b.e;}
int main()
{
    int n;
    while(cin>>n&&n)
    {
        tv* p=new tv[n];
        for(int i=0;i<n;i++)
            cin>>p[i].s>>p[i].e;
        sort(p,p+n,cmp);
        int cnt=0;
        int m=0;
        while(p[m].e<=p[n-1].s)
        {
            cnt++;
            for(int i=m+1;i<n;i++)
            {
                if(p[i].s>=p[m].e)
                {
                    m=i;break;
                }
            }
        }
        cout<<cnt+1<<endl;
        delete[]p;
    }
    return 0;
}
