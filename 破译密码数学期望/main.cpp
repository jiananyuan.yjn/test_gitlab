#include <iostream>
#include <math.h>
using namespace std;
typedef unsigned long long ull;
int main()
{
    ull n;
    cin>>n;
    if((1+n)%2==0) {
        cout<<(1+n)/2<<endl;
    } else {
        cout<<n/2+1<<endl;
    }
    return 0;
}
