#include<iostream>
#include<iomanip>
#include<cstdio>
using namespace std;
class Object{
private:
    string num,name,color,size;
    double price,tol; //商品编号、名称、颜色、尺码、单价、数量
public:
    Object(){tol=price;}
    void set(const Object& io)
    {
        //val=io.val;
        num=io.num,name=io.name,color=io.color,size=io.size,price=io.price,tol=io.tol;
    }
    Object(string a,string b,string c,string d,double e,int f)
    {
        num=a,name=b,color=c,size=d,price=e,tol=f;
    }
    Object(const Object& t);
    ~Object(){}
    //void cal_val();
    void settol(int yy){tol+=yy;}
    //void setval(double a){val+=a;}
    double getprice(){return price;}
    int gettol(){return tol;}
    double getval(){return price*tol;}  //计算总价（单价*数量）
    string get_num()const{return num;}
    void print(){cout<<name<<","<<color<<","<<size<<",";printf("%.2lf",price);cout<<","<<tol<<",";printf("%.2lf\n",getval());}
    //输出商品信息
};

class Car{
private:
    Object *p;
    int total;
    double tolval;
    int u;
    int temp;
public:
    Car(){total=0;tolval=0;u=0;}
    Car(int t){temp=t;p=new Object[t];total=0;tolval=0;u=0;}
    ~Car(){delete []p;}
    void operate(const Object& io);
    void operate(string num,string operate);
    int get_tol(){return total;}
    double get_tolp(){return tolval;}
    string get_num()const{return get_num();}
    void print();
};

int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int operatenum;
        cin>>operatenum;
        Car shop(operatenum);
        for(int i=0;i<operatenum;i++)
        {
            string operate;
            cin>>operate;
            //cin.ignore();
            if(operate=="ADD")
            {
                string num,name,color,size;
                double price;
                int n;
                cin>>num>>name>>color>>size;
                cin>>price>>n;
                Object io(num,name,color,size,price,n);
                shop.operate(io);
            }

            if(operate=="DELETE")
            {
                string num;
                cin>>num;
                shop.operate(num,operate);
            }

            if(operate=="UP")
            {
                string num;
                cin>>num;
                shop.operate(num,operate);
            }

            if(operate=="DOWN")
            {
                string num;
                cin>>num;
                shop.operate(num,operate);
            }
        }
        shop.print();
    }

    return 0;
}

void Car::operate(const Object& io)  //ADD
{
    for(int i=0;i<total;i++)
    {
        if(io.get_num()==p[i].get_num())
        {
            total++;
            p[i].settol(1);
            //p[i].setval(p[i].getprice());
            break;
        }
    }
    total++;
    for(int i=total-1;i>0;i--)
    {
        p[i].set(p[i-1]);
    }
    p[0].set(io);
    u++;  //u is king of the goods
}

void Car::operate(string num,string operate)
{
    for(int i=0;i<total;i++)
    {
        if(num==p[i].get_num())
        {
            if(operate=="DELETE")
            {
                u--;
                total-=p[i].gettol();
                p[i].settol(-p[i].gettol());
                //p[i].setval(0);
            }

            if(operate=="UP")
            {
                total++;
                p[i].settol(1);
                //p[i].setval(p[i].getprice());
            }

            if(operate=="DOWN")
            {
                total--;
                p[i].settol(-1);
                //p[i].setval(-p[i].getprice());
            }
        }
    }
}

void Car::print()
{
    cout<<"商品清单:"<<endl;
    cout<<"商品,颜色,尺码,单价,数量,小计"<<endl;

    for(int i=0;i<u;i++)
    {
        int flag=0;
        for(int j=0;j<i;j++)
        {
            if(p[j].get_num()==p[i].get_num())
            {
                flag=1;
                break;
            }
        }
        if(flag==0)
        {
            p[i].print();
            tolval+=p[i].getval();
        }
    }
    cout<<"----------"<<endl;
    cout<<total<<"件商品,总商品金额"<<fixed<<setprecision(2)<<tolval<<endl;
}

Object::Object(const Object& t)
{
    num=t.num;
    name=t.name;
    color=t.color;
    size=t.size;
    price=t.price;
    tol=1;
    //val=price;
}
