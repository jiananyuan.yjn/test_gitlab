#include<iostream>
#include<iomanip>
#include<cstdio>
using namespace std;
class Object{
private:
	string num;//商 品 编 号
	string name;//名 称
	string color;//颜 色
	string size;//尺 码
	double price;//单 价
	int o_how_many;//数 量
public:
	double total_price(){return price*o_how_many;}//计 算 总 价
	void print(){cout<<name<<","<<color<<","<<size<<",";printf("%.2lf",price);cout<<","<<o_how_many<<",";printf("%.2lf\n",total_price());}//输出商品信息
	Object(){price=0;o_how_many=0;}
	Object(string a,string b,string c,string d,double e,int f);
	Object(const Object& t);
	~Object(){}
	void set(string a,string b,string c,string d,double e,int f);
	void set_o_how_many(int a){o_how_many+=a;}
	int get_o_how_many(){return o_how_many;}
	string judge(){return num;}
	void set(const Object& r);
};

class Car{
private:
	Object *good;//商 品 对 象 集 合
	int c_how_many;//商 品 总 数
	int kindof;   //商品类数
	double shop_total_price;//购 物 车 所 有 商 品 总 价
public:
	void ADD(string a,string b,string c,string d,double e,int f);//添 加 商 品
	void DELETE(string t);//删 除 商 品
	void DOWN(string t);//减 少 商 品 数 量
	void UP(string t);//增 加 商 品 数 量
	void PRINT();//输 出 购 物 车 中 的 商 品 清 单
	Car(int tt){good=new Object[tt];kindof=0;c_how_many=0;shop_total_price=0;}
	Car(){}
	~Car(){delete[]good;}
	void set_c_how_many(int a){c_how_many+=a;}
	void get_shop_total_price();
	void set_kindof(int a){kindof+=a;}
};

int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int tt;
		cin>>tt;
		Car shop(tt);
		for(int i=0;i<tt;i++)
		{
			string operate;
			cin>>operate;

			if(operate=="ADD")
			{
				string num,name,color,size;
	            double price;
	            int n;
	            cin>>num>>name>>color>>size;
	          //     编号 名称  颜色   尺码
	            cin>>price>>n;
	          //     单价 数量
	          	shop.ADD(num,name,color,size,price,n);
			}

			if(operate=="UP")
			{
				string num;
				cin>>num;
				shop.UP(num);
			}

        	if(operate=="DOWN")
        	{
        		string num;
				cin>>num;
				shop.DOWN(num);
			}

			if(operate=="DELETE")
			{
				string num;
				cin>>num;
				shop.DELETE(num);
			}
		}
		shop.PRINT();
	}
	return 0;
}

void Object::set(const Object& t)
{
	num=t.num;//商 品 编 号
	name=t.name;//名 称
	color=t.color;//颜 色
	size=t.size;//尺 码
	price=t.price;//单 价
	o_how_many=t.o_how_many;//数 量
}

Object::Object(const Object& t)
{
	num=t.num;//商 品 编 号
	name=t.name;//名 称
	color=t.color;//颜 色
	size=t.size;//尺 码
	price=t.price;//单 价
	o_how_many=t.o_how_many;//数 量
}

void Object::set(string a,string b,string c,string d,double e,int f)
{
	num=a;//商 品 编 号
	name=b;//名 称
	color=c;//颜 色
	size=d;//尺 码
	price=e;//单 价
	o_how_many=f;//数 量
}

Object::Object(string a,string b,string c,string d,double e,int f)
{
	num=a;//商 品 编 号
	name=b;//名 称
	color=c;//颜 色
	size=d;//尺 码
	price=e;//单 价
	o_how_many=f;//数 量
}

void Car::ADD(string a,string b,string c,string d,double e,int f)
{
	Object temp(a,b,c,d,e,f);
	for(int i=0;i<kindof;i++)
	{
		if(a==good[i].judge())
		{
			UP(a);
			return ;
		}
	}
	c_how_many++;
	for(int i=kindof;i>0;i--)
		good[i].set(good[i-1]);
	good[0].set(temp);
	good[0].set_o_how_many(1);
    kindof++;
}

void Car::DELETE(string t)//删 除 商 品
{
	kindof--;
	for(int i=kindof;i>0;i--)
		good[i].set(good[i-1]);
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
		{
			c_how_many-=good[i].get_o_how_many();
			good[i].set_o_how_many(-good[i].get_o_how_many());
		}
	}
}
void Car::DOWN(string t)//减 少 商 品 数 量
{
	c_how_many--;
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
			good[i].set_o_how_many(-1);
	}
}
void Car::UP(string t)//增 加 商 品 数 量
{
    c_how_many++;
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
			good[i].set_o_how_many(1);
	}
}
void Car::PRINT()//输 出 购 物 车 中 的 商 品 清 单
{
	cout<<"商品清单:"<<endl;
    cout<<"商品,颜色,尺码,单价,数量,小计"<<endl;
	for(int i=0;i<kindof;i++)
		good[i].print();

    cout<<"----------"<<endl;
    cout<<c_how_many<<"件商品,总商品金额"<<fixed<<setprecision(2)<<shop_total_price<<endl;
}

void Car::get_shop_total_price()
{
	for(int i=0;i<kindof;i++)
		shop_total_price+=(good[i].total_price());
}
