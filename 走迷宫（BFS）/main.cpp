#include <bits/stdc++.h>
using namespace std;
int Map[17][17],book[17][17],ans[400];
int n,m,x,y,p,q,u;
int next[4][2]={{0,-1},{-1,0},{0,1},{1,0}};
struct point {
    int x;
    int y;
    int step;
    point(int a,int b,int c):x(a),y(b),step(c){}
};
queue<point> v;
int bfs()
{
    book[x][y]=1;
    v.push(point(x,y,0));
    while(!v.empty()) {
        int tx=v.front().x;
        int ty=v.front().y;
        for(int k=0;k<4;k++) {
            tx+=next[k][0];
            ty+=next[k][1];
            if(tx<1||ty<1||tx>n||ty>m) {
                tx-=next[k][0];
                ty-=next[k][1];
                continue;
            }
            if(book[tx][ty]==0 && Map[tx][ty]==1 ) {
                book[tx][ty]=1;
                v.push(point(tx,ty,v.front().step+1));
            }
            if(tx==p && ty==q ) {
                return v.front().step+1;
            }
            tx-=next[k][0];
            ty-=next[k][1];
        }
        v.pop();
    }
    return -1;
}
int main()
{
    cin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int  j=1;j<=m;j++)
            cin>>Map[i][j];
    cin>>x>>y;
    cin>>p>>q;
    cout<<bfs()<<endl;
    return 0;
}
