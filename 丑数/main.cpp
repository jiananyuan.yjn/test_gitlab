#include <iostream>
#define MAX (int)(2e6+2)
using namespace std;
typedef long long ll;
ll a[MAX];
int main() {
    ll l,r;
    cin>>l>>r;
    int cnt=0;
    ll dis=r-l+1;
    ll tmp=l;
    for(int i=1;i<=dis/2;i++) {
        if(a[i]==0) {
            for(int j=1;i+j*tmp<=dis;j++)
                a[i+j*tmp]=1;
        }
        tmp++;
    }
    for(int i=1;i<=dis;i++) {
        cnt+=(a[i]==0);
    }
    cout<<cnt<<endl;
    return 0;
}
