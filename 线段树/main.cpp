#include <iostream>
#include <stdio.h>
using namespace std;
typedef long long ll;
ll n,a[100005],d[270000],b[270000];
void build(ll l,ll r,ll pos) {
    if(l == r) {
        d[pos] = a[l];
        return ;
    }
    ll mid = (l+r)>>1;
    build(l, mid, pos << 1);
    build(mid + 1,r,(pos << 1) | 1);
    d[pos] = d[pos << 1] + d[(pos << 1 | 1)];
}

void update(ll l,ll r,ll c,ll s,ll t,ll pos) {
    if(l <= s && t <= r) {
        d[pos] += (t-s+1)*c;
        b[pos] += c;
        return ;
    }
    ll mid = (s+t) >> 1;
    if(b[pos]) {
        d[pos<<1] += b[pos]*(mid-s+1);
        d[(pos<<1 | 1)] += b[pos]*(t-mid);
        b[pos<<1] += b[pos];
        b[(pos<<1) | 1] += b[pos];
    }
    b[pos] = 0;
    if(l <= mid) update(l,r,c,s,mid,pos << 1);
    if(r > mid)  update(l,r,c,mid+1,t,(pos << 1)|1);
    d[pos] = d[pos<<1] + d[(pos<<1) | 1];
}

ll getsum(ll l,ll r,ll s,ll t,ll pos) {
    if(l <= s && t <= r) return d[pos];
    ll mid = (s+t)>>1;
    if(b[pos]) {
        d[pos<<1] += b[pos]*(mid-s+1);
        d[(pos<<1 | 1)] += b[pos]*(t-mid);
        b[pos<<1] += b[pos];
        b[(pos<<1) | 1] += b[pos];
    }
    b[pos] = 0;
    ll sum = 0;
    if(l <= mid) sum = getsum(l,r,s,mid,pos << 1);
    if(r > mid)  sum += getsum(l,r,mid+1,t,(pos << 1)|1);
    return sum;
}

int main() {
    ll q; scanf("%lld%lld",&n,&q);
    for(ll i = 1; i<=n; i++) scanf("%lld",&a[i]);
    build(1,n,1);
    while(q--) {
        int op; scanf("%d",&op);
        ll x,y; scanf("%lld%lld",&x,&y);
        if(op == 2) {
            printf("%lld\n",getsum(x,y,1,n,1));
        }
        else {
            ll k; scanf("%lld",&k);
            update(x,y,k,1,n,1);
        }
    }
    return 0;
}
