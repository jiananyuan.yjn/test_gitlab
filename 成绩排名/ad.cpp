#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;
struct people {
	int num;
	int score;
};
bool cmp(people& a,people& b) {
	if(a.score > b.score) return true;
	else if(a.score < b.score) return false;
	else {
		return a.num < b.num;
	}
}
int main() {
	int n,m;
	cin>>n>>m;
	people p[n+1];
	for(int i=1;i<=n;i++)
		cin>>p[i].num>>p[i].score;
	sort(p+1,p+n+1,cmp);
	int ans_score=p[int(1.5*m)].score,ans_many=int(1.5*m);
	for(int i=int(1.5*m)+1;i<=n;i++) {
		if(p[i].score == p[i-1].score ) {
			ans_score=p[i].score;
			ans_many++;
		} else {
			break;
		}
	}
	cout<<ans_score<<" "<<ans_many<<endl;
	for(int i=1;i<=ans_many;i++) {
		cout<<p[i].num<<" "<<p[i].score<<endl;
	}
	return 0;
}
