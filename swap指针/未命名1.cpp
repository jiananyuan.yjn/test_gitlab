#include<iostream>
using namespace std;
void swap(int *a,int *b);
int main()
{
	int x,y,*a=&x,*b=&y;
	cin>>*a>>*b;
	swap(a,b);
	cout<<*a<<' '<<*b;
} 

void swap(int *a,int *b)
{
	int t=*a;
	*a=*b;
	*b=t;
}
