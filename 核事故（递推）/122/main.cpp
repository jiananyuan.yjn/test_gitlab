#include<iostream>
using namespace std;
int a[1005];
int M(int k){
    int ans=1,tmp=2;
    while(k>0){
        if(k&1)  ans*=tmp;
        tmp*=tmp;
        k>>=1;
    }
    return ans;
}
int main(){
    a[1]=a[2]=0; a[3]=1;
    for(int i=4;i<=30;i++)  a[i]=a[i-1]+a[i-2]+a[i-3]+M(i-3);
    int n;
    while(cin>>n&&n)    cout<<a[n]<<endl;
    return 0;
}
