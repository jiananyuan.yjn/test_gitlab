#include<iostream>
#include<iomanip>
#include<string>
#include<cstdio>
using namespace std;
class Object{
private:
	string num;
	string name;
	string color;
	string size;
	double price;
	int o_how_many;
public:
	double total_price(){return price*o_how_many;}//¼Æ Ëã ×Ü ¼Û
	void print(){cout<<name<<","<<color<<","<<size<<",";printf("%.2lf",price);cout<<","<<o_how_many<<",";printf("%.2lf\n",total_price());}//Êä³öÉÌÆ·ÐÅÏ¢
	Object(){price=0;o_how_many=0;}
	Object(string a,string b,string c,string d,double e,int f);
	Object(const Object& t);
	~Object(){}
	void set(string a,string b,string c,string d,double e,int f);
	void set_o_how_many(int a){o_how_many+=a;}
	int get_o_how_many(){return o_how_many;}
	string judge(){return num;}
	void set(const Object& r);
};

class Car{
private:
	Object *good;
	int c_how_many;
	int kindof;
	double shop_total_price;
public:
	void ADD(string a,string b,string c,string d,double e,int f);
	void DELETE(string t);
	void DOWN(string t);
	void UP(string t);
	void PRINT();
	Car(int tt){good=new Object[tt];c_how_many=0;shop_total_price=0;kindof=0;}
	Car(){}
	~Car(){delete[]good;}
	void set_c_how_many(int a){c_how_many+=a;}
	void get_shop_total_price();
	void set_kindof(int a){kindof+=a;}
};

int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int tt;
		cin>>tt;
		Car shop(tt);
		for(int i=0;i<tt;i++)
		{
			string operate;
			cin>>operate;

			if(operate=="ADD")
			{
				string num,name,color,size;
	            double price;
	            int n;
	            cin>>num>>name>>color>>size;
	            cin>>price>>n;
	          	shop.ADD(num,name,color,size,price,n);
			}

			if(operate=="UP")
			{
				string num;
				cin>>num;
				shop.UP(num);
			}

        	if(operate=="DOWN")
        	{
        		string num;
				cin>>num;
				shop.DOWN(num);
			}

			if(operate=="DELETE")
			{
				string num;
				cin>>num;
				shop.DELETE(num);
			}
		}
		shop.PRINT();
	}
	return 0;
}

void Object::set(const Object& t)
{
	num=t.num;
	name=t.name;
	color=t.color;
	size=t.size;
	price=t.price;
	o_how_many=t.o_how_many;
}

Object::Object(const Object& t)
{
	num=t.num;
	name=t.name;
	color=t.color;
	size=t.size;
	price=t.price;
	o_how_many=t.o_how_many;
}

void Object::set(string a,string b,string c,string d,double e,int f)
{
	num=a;
	name=b;
	color=c;
	size=d;
	price=e;
	o_how_many=f;
}

Object::Object(string a,string b,string c,string d,double e,int f)
{
	num=a;
	name=b;
	color=c;
	size=d;
	price=e;
	o_how_many=f;
}

void Car::ADD(string a,string b,string c,string d,double e,int f)
{
	Object temp(a,b,c,d,e,f);
	for(int k=0;k<kindof;k++)
	{
		if(a==good[k].judge())
		{
			UP(a);
			return ;
		}
	}
	kindof++;
	c_how_many++;
	for(int i=kindof;i>0;i--)
		good[i].set(good[i-1]);
	good[0].set(temp);
}

void Car::DELETE(string t)
{
	kindof--;
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
		{
			c_how_many-=good[i].get_o_how_many();
			good[i].set_o_how_many(-good[i].get_o_how_many());
			for(int j=i;j<kindof-1;j++)
			{
				good[j].set(good[j+1]);
			}
		}
	}
}

void Car::DOWN(string t)
{
	c_how_many--;
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
		{
			if(good[i].get_o_how_many()==1)
				return ;
			good[i].set_o_how_many(-1);
		}
	}
}
void Car::UP(string t)
{
	c_how_many++;
	for(int i=0;i<kindof;i++)
	{
		if(good[i].judge()==t)
		{
			good[i].set_o_how_many(1);
			break;
		}
	}
}
void Car::PRINT()
{
	cout<<"商品清单:"<<endl;
    cout<<"商品,颜色,尺码,单价,数量,小计"<<endl;
	for(int i=0;i<kindof;i++)
		good[i].print();

    cout<<"----------"<<endl;
    cout<<c_how_many<<"件商品,总商品金额"<<fixed<<setprecision(2)<<shop_total_price<<endl;
}

void Car::get_shop_total_price()
{
	for(int i=0;i<kindof;i++)
		shop_total_price+=(good[i].total_price());
}
