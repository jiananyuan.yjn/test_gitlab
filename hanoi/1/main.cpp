#include<iostream>
#include<cstdio>
using namespace std;
void hanoi(int n,char A,char B,char C);
void moving(char x,char y);
int main()
{
    int n;
    cin>>n;
    hanoi(n,'A','B','C');
}

void hanoi(int n,char A,char B,char C)
{
    if(n==1)
        moving(A,C);
    else
    {
        hanoi(n-1,A,C,B);
        moving(A,C);
        hanoi(n-1,B,A,C);
    }
}

void moving(char x,char y)
{
    printf("%c--->%c\n",x,y);
}
