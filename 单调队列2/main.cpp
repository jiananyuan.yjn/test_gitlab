#include <iostream>
#include <queue>
using namespace std;
const int maxn = 1000005;
int n,k;
int a[maxn];
deque<int> q;

int main() {
    cin>>n>>k;
    for(int i=0;i<n;i++) cin>>a[i];
    int p = 0;
    while(p<n) {
        while(!q.empty()&&a[p]<=a[q.back()]) q.pop_back();
        q.push_back(p);
        while(!q.empty()&&q.front()<p-k+1) q.pop_front();
        if(p>=k-1) cout<<a[q.front()]<<" ";
        p++;
    }
    cout<<endl;
    p=0;
    q.clear();
    while(p<n) {
        while(!q.empty()&&a[p]>=a[q.back()]) q.pop_back();
        q.push_back(p);
        while(!q.empty()&&q.front()<p-k+1) q.pop_front();
        if(p>=k-1) cout<<a[q.front()]<<" ";
        p++;
    }
    return 0;
}
