#include <iostream>
using namespace std;

int main() {
    int n,a,b;
    while(cin>>n>>a>>b) {
        if(a>b) swap(a,b);
        int mid = n/2;
        if(a<=mid&&b>mid)
            cout<<"Final!"<<endl;
        else {
            int ans = 0;
            while(a!=b) {
                a = (a+1)/2;
                b = (b+1)/2;
                ans++;
            }
            cout << ans << endl;
        }
    }
    return 0;
}

//            if(a%2==0&&b%2==0) {
//                while(d) {
//                    d/=2; ans++;
//                }
//            }
//            else if(a%2!=0&&b%2==0) {
//                d--;
//                while(d) {
//                    d/=2; ans++;
//                }
//            }
//            else if(a%2==0&&b%2!=0) {
//                d++;
//                while(d) {
//                    d/=2; ans++;
//                }
//            }
//            else if(a%2!=0&&b%2!=0) {
//                while(d) {
//                    d/=2; ans++;
//                }
//            }
