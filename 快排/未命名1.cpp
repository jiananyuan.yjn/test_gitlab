#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
void Quick_sort (int v[],int left,int right);
int main()
{
	int n;
	cin>>n;
	int v[100000];
	for(int i=0;i<n;i++)
		cin>>v[i];
	Quick_sort (v,0,n-1);
	for(int i=0;i<n;i++)
		cout<<v[i]<<' ';
}

void Quick_sort (int v[],int left,int right)
{
	int acord=v[right],l=left,r=right;
	while(l<r)
	{
		swap(v[l],v[r]);
		while(r>l && v[r]>acord )
			r--;
		while(l<r && v[l]<=acord )
			l++;
	}
	swap(v[left],v[r]);
	if(left < r-1)
		Quick_sort(v,left,r-1);
	if(r+1 <right)
		Quick_sort (v,r+1,right);
}
