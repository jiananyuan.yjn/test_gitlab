#include <iostream>
#include<cstring>
using namespace std;
const int mod=9973;
int n;
#define maxn 12
struct Matrix{
    int m[maxn][maxn];
    Matrix()
    {
        for(int i=0;i<maxn;i++)
            for(int j=0;j<maxn;j++)
                m[i][j]=1;
    }
};
Matrix multiply(Matrix a, Matrix b)
{
    Matrix ans;
    memset(ans.m,0,sizeof(ans.m));
    for(int i = 0;i < n; i++ )
    {
        for(int j = 0;j < n; j++ )
        {
            for(int k = 0;k < n;k++)
            {
                ans.m[i][j] += a.m[i][k] * b.m[k][j] ;
                ans.m[i][j] %= mod;
            }
        }
    }
    return ans;
}

Matrix pow_matrix(Matrix a,int m)
{
    Matrix res;
    memset(res.m,0,sizeof(res.m));
    for(int i=0;i<n;i++)
        res.m[i][i]=1;
    while(m)
    {
        if(m%2)
            res=multiply(res,a);
        a=multiply(a,a);
        m/=2;
    }
    return res;
}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int mi;
        cin>>n>>mi;
        Matrix a;
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                cin>>a.m[i][j];
        a=pow_matrix(a,mi);
        int s=0;
        for(int i=0;i<n;i++)
        {
            s+=a.m[i][i];
            s%=mod;
        }
        cout<<s<<endl;
    }
    return 0;
}
