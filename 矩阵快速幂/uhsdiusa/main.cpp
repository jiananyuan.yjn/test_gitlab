#include <iostream>
#include <cstring>
using namespace std;
typedef long long LL;
int n;
const int MAX=12;
const int mod=9973;
struct Matrix{
    int a[MAX][MAX];
    Matrix()
    {
        for(int i=0;i<12;i++)
            for(int j=0;j<12;j++)
                a[i][j]=1;
    }
    Matrix operator*(Matrix b)
    {
        Matrix rec;
        memset(rec.a,0,sizeof(rec.a));
        for(int i=1;i<=n;i++)
        {
            for(int j=1;j<=n;j++)
            {
                for(int k=1;k<=n;k++)
                {
                    rec.a[i][j]=(rec.a[i][j]+a[i][k]*b.a[k][j])%mod;
                }
            }
        }
        return rec;
    }
    Matrix pow(Matrix m,int k)
    {
        Matrix ans;
        memset(ans.a,0,sizeof(ans.a));
        for(int i=1;i<=n;i++)
            ans.a[i][i]=1;
        while(k)
        {
            if(k&1)
            {
                ans=ans*m;
            }
            m=m*m;
            k>>=1;
        }
        return ans;
    }
};
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int k;
        cin>>n>>k;
        Matrix m;
        for(int i=1;i<=n;i++)
            for(int j=1;j<=n;j++)
                cin>>m.a[i][j];
        m=m.pow(m,k);
        LL sum=0;
        for(int i=1;i<=n;i++)
                sum=(sum+m.a[i][i])%mod;
        cout<<sum<<endl;
    }
    return 0;
}
