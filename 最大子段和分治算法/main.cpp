#include <iostream>
#include <algorithm>
#include <time.h>
#include <fstream>
#define MAX 10000002
using namespace std;
clock_t start,stop;
double duration;
int arr[MAX];
int solve(int* a,int l,int r) {
    if(l==r) {
        if(a[l]>0) return a[l];
        else       return 0;
    }
    int mid = (l+r)/2;
    int MaxLeftSum = solve(a,l,mid);
    int MaxRightSum = solve(a,mid+1,r);

    int MaxLeftBorderSum = 0 ;
    int MaxRightBorderSum = 0;
    int tmp = 0;
    for(int i=mid;i>=l;i--) {
        tmp += a[i];
        MaxLeftBorderSum = max(MaxLeftBorderSum,tmp);
    }

    tmp = 0;
    for(int i=mid+1;i<=r;i++) {
        tmp += a[i];
        MaxRightBorderSum = max(MaxRightBorderSum,tmp);
    }

    return max(MaxLeftBorderSum+MaxRightBorderSum,max(MaxLeftSum,MaxRightSum));
}

int main()
{
    int n; cin>>n;
    ifstream in("in.txt");
    for(int i=1;i<=n;i++) in>>arr[i];
    in.close();
    start = clock();
    cout<<solve(arr,1,n)<<endl;
    stop = clock();
    duration = ((double)(stop-start))/CLK_TCK;
    cout<<duration<<endl;
    return 0;
}
