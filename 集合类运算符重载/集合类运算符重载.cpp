#include<iostream>
#include<vector>
using namespace std;
class CSet{
protected:
	int n;//集合元素个数
	int *p; 
public:	
	CSet(){}
	CSet(int _n,int *_p):n(_n)
	{
		n=_n;
		p=new int[n];
		for(int i=0;i<n;i++)
			p[i]=_p[i];
	}
	CSet(const CSet& t)
	{
		n=t.n;
		p=new int[n];
		for(int i=0;i<n;i++)
			p[i]=t.p[i];
	}
	void display()
	{
		cout<<p[0];
		for(int i=1;i<n;i++)
			cout<<' '<<p[i];
		cout<<endl;
	}
	CSet operator+ (const CSet& t)
	{
		vector<int> v;
		for(int i=0;i<n;i++)
			v.push_back(p[i]);
			
		for(int i=0;i<t.n;i++)
		{
			int flag=0;
			for(int j=0;j<v.size();j++)
			{
				if(t.p[i]==v[j])
				{
					flag=1;
					break;
				}
			}
			if(!flag)
				v.push_back(t.p[i]);
		}
		
		int newn=v.size();
		int *q=new int[newn];
		for(int i=0;i<newn;i++)
			q[i]=v[i];
		return CSet(newn,q);
	}
	CSet operator- (const CSet& t)
	{
		vector<int> v;
		for(int i=0;i<n;i++)
		{
			int flag=0;
			for(int j=0;j<t.n;j++)
			{
				if(p[i]==t.p[j])
				{
					flag=1;
					break;
				}
			}
			if(!flag)
				v.push_back(p[i]);
		}
		
		int newn=v.size();
		int *q=new int[newn];
		for(int i=0;i<newn;i++)
			q[i]=v[i];
		return CSet(newn,q);
	}
	CSet operator* (const CSet& t)
	{
		vector<int> v;
		for(int i=0;i<n;i++)
		{
			int flag=0;
			for(int j=0;j<t.n;j++)
			{
				if(p[i]==t.p[j])
				{
					flag=1;
					break;
				}
			}
			if(flag)
				v.push_back(p[i]);
		}
		
		int newn=v.size();
		int *q=new int[newn];
		for(int i=0;i<newn;i++)
			q[i]=v[i];
		return CSet(newn,q);
	}	
	~CSet()
	{
		delete []p;
	}
};
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		int *p=new int[n];
		for(int i=0;i<n;i++)
			cin>>p[i];
		CSet a(n,p);
		delete[]p;
		cin>>n;
		int *q=new int[n];
		for(int i=0;i<n;i++)
			cin>>q[i];
		CSet b(n,q);
		delete[]q;
		cout<<"A:";
		a.display();
		cout<<"B:";
		b.display();
		cout<<"A+B:";
		CSet t1=a+b;
		t1.display();
		cout<<"A*B:";
		CSet t2=a*b;
		t2.display();
		cout<<"(A-B)+(B-A):";
		CSet t3=(a-b)+(b-a);
		t3.display();
		cout<<endl;
	}
	return 0;
}
