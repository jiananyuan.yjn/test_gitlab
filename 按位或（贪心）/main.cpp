#include <iostream>
#include<bitset>
using namespace std;
#define ll unsigned long long
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        ll a,b;
        cin>>a>>b;
        if(a==b)
            cout<<a<<endl;
        else
        {
            int ci=0;
            ll ta=a,tb=b;
            while(ta!=tb&&tb>0)
            {
                ta=ta>>1;
                tb=tb>>1;
                ci++;
            }
            ll ans=(ta<<ci)+((ll)1<<ci)-1;
            cout<<ans<<endl;
        }

    }
    return 0;
}
