#include <iostream>
#include <cstring>
#define MAX 1000+5
using namespace std;
char p[MAX],s[MAX];
int Next[MAX],ls,lp;
void setNext() {
    int j = 0;
    for(int i = 2; i <= lp; i++) {
        while(j && p[i]!=p[j+1]) j = Next[j];
        if(p[j+1] == p[i]) j++;
        Next[i] = j;
    }
}
void kmp() {
    ls = strlen(s+1);
    lp = strlen(p+1);
    setNext();
    int j = 0;
    for(int i = 1; i <= ls; i++) {
        while(j>0 && p[j+1] != s[i]) j = Next[j];
        if(p[j+1] == s[i]) j++;
        if(j==lp) {
            cout<<i-j+1<<endl;
            j = Next[j];
        }
    }
}

int main() {
    cin>>(s+1);
    cin>>(p+1);
    kmp();
    return 0;
}
