#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    int L;
    cin>>L;
    L++;
    int n;
    cin>>n;
    int maxv=0,minv=0;
    while(n--) {
        int x;
        cin>>x;
        minv=max(minv,min(x,L-x));
        maxv=max(maxv,max(x,L-x));
    }
    cout<<minv<<" "<<maxv<<endl;
    return 0;
}
