#include <iostream>
#include <string>
using namespace std;
typedef long long LL;
string n;
int m;
int bigmod(int len) {
    if(!len) {
        return (n[len]-'0');
    }
    return ( (LL)((10*bigmod(len-1))%m) + (n[len]-'0')%m )%m;
}

int main()
{
    cin>>n>>m;
    cout<<bigmod(n.length()-1)<<endl;
    return 0;
}
