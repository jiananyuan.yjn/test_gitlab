#include<iostream>
using namespace std;
class CPeople{
protected:
    string id,name;
public:
    CPeople(string _id="123",string _name="yuan"):
        id(_id),name(_name){}
};

class CInternetUser:virtual public CPeople{
protected:
    string password;
public:
    CInternetUser():password("123"){}
    CInternetUser(string _id,string _name,string _password):
        CPeople(_id,_name),password(_password){}
    void registerUser(string _id,string _name,string _password)
    {
        id=_id;
        name=_name;
        password=_password;
    }
    bool login(string _id,string _password)
    {
        if(id==_id&&password==_password)
            return true;
        return false;
    }
    virtual void print()
    {
        cout<<"Name: "<<name<<" ID: "<<id<<endl;
    }
};

class CBankCustomer:virtual public CPeople{
protected:
    double balance;
public:
    CBankCustomer():balance(0){}
    void openAccount(string _name,string _id)
    {
        name=_name;
        id=_id;
    }
    void deposit(double m)
    {
        balance+=m;
    }
    bool withdraw(double m)
    {
        if(balance<m)
            return false;
        balance-=m;
        return true;
    }
    virtual void print()
    {
        cout<<"Bank balance: "<<balance<<endl;
    }
};

class CInternetBankCustomer:public CInternetUser,public CBankCustomer{
protected:
    double balance;
    double front_balance;
    double today_profit;
    double today_million_profit;
    double last_million_profit;
public:
    CInternetBankCustomer():CBankCustomer(),balance(0),today_profit(0){}
    bool deposit(double m)
    {
        if(CBankCustomer::balance<m)
            return false;
        balance+=m;
        CBankCustomer::balance-=m;
        return true;
    }
    bool withdraw(double m)
    {
        if(CInternetBankCustomer::balance<m)
            return false;
        balance-=m;
        CBankCustomer::balance+=m;
        return true;
    }
    void setInterest(double m)
    {
        today_profit=m;
    }

    void calculateProfit()
    {
        balance+=(balance/10000)*today_profit;
    }
    bool login(string _id,string _password)
    {
        if(id==_id&&password==_password&&CBankCustomer::name==CInternetUser::name&&CBankCustomer::id==CInternetUser::id)
            return true;
        return false;
    }
    void print()
    {
        CInternetUser::print();
        CBankCustomer::print();
        cout<<"Internet bank balance: "<<balance<<endl;
    }
};

int main()
{
    int t, no_of_days, i;
    string i_xm,i_id,i_mm,b_xm,b_id,ib_id,ib_mm;
    double money, interest;
    char op_code;
    cin >> t;
    while (t--)
    {
        cin >> i_xm >> i_id >> i_mm;
        cin >> b_xm >> b_id;
        cin >> ib_id >> ib_mm;
        CInternetBankCustomer ib_user;
        ib_user.registerUser(i_xm, i_id, i_mm);
        ib_user.openAccount(b_xm, b_id);
        if (ib_user.login(ib_id, ib_mm) == 0)
        {
            cout << "Password or ID incorrect" << endl;
            continue;
        }
        cin >> no_of_days;
        for (i=0; i < no_of_days; i++)
        {
            cin >> op_code >> money >> interest;
            ib_user.calculateProfit();
            switch (op_code)
            {
                case 'S':
                case 's':
                    if (ib_user.deposit(money) == 0)
                    {
                        cout << "Bank balance not enough" << endl;
                        continue;
                    }
                    break;

                case 'T':
                case 't':
                    if (ib_user.withdraw(money) == 0)
                    {
                        cout << "Internet bank balance not enough" << endl;
                        continue;
                    }
                    break;

                case 'D':
                case 'd':
                    ib_user.CBankCustomer::deposit(money);
                    break;

                case 'W':
                case 'w':
                    if (ib_user.CBankCustomer::withdraw(money) == 0)
                    {
                        cout << "Bank balance not enough" << endl;
                        continue;
                    }
                    break;

                default:
                    cout << "Illegal input" << endl;
                    continue;
            }
            ib_user.setInterest(interest);
            ib_user.print();
            cout<<endl;
      }
   }
}
