#include <iostream>
#define NIL -1
using namespace std;
struct node {
    int parent;
    int left;
    int right;
};
node tree[10000];
void Preorder(int ND) {
    if(ND==NIL) return ;
    cout<<" "<<ND;
    Preorder(tree[ND].left);
    Preorder(tree[ND].right);
}

void Inorder(int ND) {
    if(ND==NIL) return ;
    Inorder(tree[ND].left);
    cout<<" "<<ND;
    Inorder(tree[ND].right);
}

void Postorder(int ND) {
    if(ND==NIL) return ;
    Postorder(tree[ND].left);
    Postorder(tree[ND].right);
    cout<<" "<<ND;
}
int main()
{
    int t;
    cin>>t;
    //initialize
    for(int i=0;i<t;i++)
        tree[i].parent=NIL;

    for(int i=0;i<t;i++) {
        int p;
        cin>>p;
        int l,r;
        cin>>l>>r;
        tree[p].left=l;
        tree[p].right=r;
        if(l!=NIL)  tree[l].parent=p;
        if(r!=NIL)  tree[r].parent=p;
    }

    int root;
    for(int i=0;i<t;i++) {
        if(tree[i].parent==NIL) {
            root=i;
            break;
        }
    }

    cout<<"Preorder"<<endl;
    Preorder(root);
    cout<<endl<<"Inorder"<<endl;
    Inorder(root);
    cout<<endl<<"Postorder"<<endl;
    Postorder(root);
    cout<<endl;
    return 0;
}
