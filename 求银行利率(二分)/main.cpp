#include<cstdio>
#include<cmath>
#include<cstdlib>
const double eps=1e-4;
int borrow,per,month;
void binary(double l,double r) {
    double sum=0,t=(l+r)/2;
    for(int i=1;i<=month;i++) {
        sum+=pow(1/t,i);
    }
    if( fabs(sum*per-borrow) < eps ) {
        printf("%.1lf",(t-1)*100);
        exit (0);
    }
    if( sum*per > borrow )
        binary(t,r);
    if( sum*per < borrow )
        binary(l,t);
}
int main() {
    scanf("%d%d%d",&borrow,&per,&month);
    binary(1,5);
}
