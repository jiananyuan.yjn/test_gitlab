#include <iostream>
using namespace std;
struct node{
    int key;
    node *l,*r,*p;
} *root,*NIL;

void inorder(node *t) {
    if(t==NIL)  return ;
    inorder(t->l);
    cout<<" "<<t->key;
    inorder(t->r);
}
void preorder(node *t) {
    if(t==NIL)  return ;
    cout<<" "<<t->key;
    preorder(t->l);
    preorder(t->r);
}
void Print() {
    inorder(root);
    cout<<endl;
    preorder(root);
    cout<<endl;
}
void Insert(int k) {
    node *t=new node;
    t->key=k;
    t->l=t->r=t->p=NIL;
    node *x=root;
    node *y=NIL;
    while(x!=NIL) {
        y=x;
        if(t->key < x->key )
            x=x->l;
        else
            x=x->r;
    }
    t->p=y;
    if(y==NIL)
        root=t;
    else {
        if( t->key > y->key )
            y->r=t;
        else
            y->l=t;
    }
}
node* Find(int k) {
    node *ans=root;
    while(ans!=NIL && k!=ans->key) {
        if(k > ans->key )
            ans=ans->r;
        else
            ans=ans->l;
    }
    return ans;
}
node* minimum(node* x) {
    while(x->l!=NIL)    x=x->l;
    return x;
}
node* successor(node* x) {
    if(x->r!=NIL)   return minimum(x->r);
//    node *y= x->p ;
//    while(y!=NIL && x==y->r) {
//        x=y;
//        y=y->p;
//    }
//    return y;
}
void Delete(node *z) {
    node *y,*x;
    //y为要删除的对象，x为y的子结点
    //确定要删除的节点
    if( z->l==NIL || z->r==NIL ) y=z;//叶结点、单子结点
    else                         y=successor(z);
    //确定y的子结点x
    if(y->l!=NIL)//有左子
        x=y->l;
    else         //没有左子
        x=y->r;
    if(x!=NIL)
        x->p = y->p;

    if(y->p == NIL )    //设置parent的孩子
        root=x;
    else {
        if(y== y->p->l )
            y->p->l = x ;
        else
            y->p->r = x;
    }

    if(y!=z)
        z->key=y->key;//赋值覆盖
    delete y;
}
int main()
{
    int t;
    cin>>t;
    while(t--) {
        string op;
        cin>>op;
        if(op=="insert") {
            int des;
            cin>>des;
            Insert(des);
        }

        if(op=="find") {
            int des;
            cin>>des;
            if(Find(des)!=NIL)   cout<<"yes"<<endl;
            else                 cout<<"no" <<endl;
        }

        if(op=="delete") {
            int des;
            cin>>des;
            node* ans=Find(des);
            if(ans!=NIL)
                Delete(ans);
        }
        if(op=="print")     Print();
    }
    return 0;
}
