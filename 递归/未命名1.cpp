#include<iostream>
using namespace std;
int F(int n);

int main()
{
	int n;
	cin>>n;
	cout<<F(n)<<endl;
	return 0;
}

int F(int n)
{
	if(n==1)
		return 1;
	return n*F(n-1);
}
