#include <iostream>
using namespace std;
struct node{int p,l,r;};
#define MAX 100002
#define NIL -1
node T[MAX];
int D[MAX];
void setDepth(int u,int p)
{
    D[u]=p;
    if(T[u].l!=NIL)
        setDepth(T[u].l,p+1);
    if(T[u].r!=NIL)
        setDepth(T[u].r,p);
}
void print(int i)
{
    cout<<"node "<<i<<": parent = ";
    cout<<T[i].p<<", depth = ";
    cout<<D[i]<<", ";
    if(T[i].p==NIL)
        cout<<"root, [";
    else if(T[i].l==NIL)
        cout<<"leaf, [";
    else
        cout<<"internal node, [";
    for(int j=0,c=T[i].l;c!=NIL;j++,c=T[c].r)
    {   //先初始化最左侧儿子，之后递推求兄弟
        if(j)
            cout<<", ";
        cout<<c;
    }
    cout<<"]"<<endl;
}
int main()
{
    int t,temp,root;
    cin>>t;
    for(int i=0;i<t;i++)
        T[i].p=T[i].l=T[i].r=NIL;
    //所有结点进行初始化为NIL
    for(int j=0;j<t;j++)
    {
        int idx,n;
        cin>>idx>>n;
        for(int i=0;i<n;i++)
        {
            int x;
            cin>>x;
            if(i==0)
                T[idx].l=x;//根据左子右兄弟表示法，只要确定了
            else           //最左侧的儿子，剩余的兄弟递推过去
                T[temp].r=x;//就可以了，因此新开了一个temp变量
            temp=x;
            T[x].p=idx;     //设置父亲
        }
    }
    for(int i=0;i<t;i++)
        if(T[i].p==NIL)
        {
            root=i;
            break;
        }                   //找根节点
    setDepth(root,0);
    for(int i=0;i<t;i++)
        print(i);
    return 0;
}
