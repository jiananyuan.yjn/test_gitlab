#include <iostream>
#include<string>
#include<algorithm>
using namespace std;
int main()
{
    int n;
    cin>>n;
    string *p=new string[n];
    for(int i=0;i<n;i++)
        cin>>p[i];
    string ans=p[0];
    for(int i=1;i<n;i++)
    {
        int pos=0;
        for(int j=0;j<p[i].length();j++)
        {
            string tmp=p[i].substr(0,p[i].length()-j);
            if(ans.rfind(tmp)!=string::npos&&ans[ans.rfind(tmp)+tmp.length()]=='\0'&&tmp.length()!=p[i].length())
            {
                pos=p[i].length()-j;
                break;
            }
        }
        ans+=p[i].substr(pos);
    }
    cout<<ans<<endl;
    delete[]p;
    return 0;
}
