#include<bits/stdc++.h>
int isPalindrome(char s[]);
using namespace std;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		char s[100];
		cin>>s;
		int flag=isPalindrome(s);
	
		if(flag==1)
			cout<<"Yes"<<endl;
		else
			cout<<"No"<<endl;
	} 
}

int isPalindrome(char s[])
{
	char temp[100];
	strcpy(temp,s);
	reverse(temp,temp+strlen(temp));
	if(strcmp(temp,s)==0)
		return 1;
	else
		return 0;
}
