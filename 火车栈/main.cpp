#include<iostream>
#include<stack>
int a[1000];
using namespace std;
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        stack<char> in;
        stack<char> out;
        int n,u=0;
        cin>>n;
        char *p=new char[n];
        char *q=new char[n];
        cin>>p>>q;
        for(int i=n-1;i>=0;i--)
            out.push(q[i]);
        for(int i=0;i<n;i++)
        {
            in.push(p[i]);
            a[u++]=1;
            while(!in.empty()&&in.top()==out.top())
            {
                in.pop();
                out.pop();
                a[u++]=0;
            }
        }
        if(in.empty()&&out.empty())
        {
            cout<<"Yes"<<endl;
            for(int i=0;i<2*n;i++)
            {
                if(a[i]==1)
                    cout<<"in"<<endl;
                else
                    cout<<"out"<<endl;
            }
        }
        else
        {
            cout<<"No"<<endl;
        }
        cout<<"FINISH"<<endl;
        delete[]p;
        delete[]q;
    }
    return 0;
}
