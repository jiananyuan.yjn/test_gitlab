#include<iostream>
using namespace std;
char a[6][6]={{'O','-','O','-','O','O'},
			  {'#','O','O','#','#','#'},
			  {'O','O','O','-','#','-'},
			  {'#','#','#','O','-','-'},
			  {'-','-','-','#','-','#'},
			  {'-','-','-','-','-','-'}};
int judge1()  //行 ,返回1表示player1 胜，返回2表示player2胜 
{
	for(int i=0;i<6;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(a[i][j]=='O'&&a[i][j+1]=='O'&&a[i][j+2]=='O'&&a[i][j+3]=='O')
				return 1;
			if(a[i][j]=='#'&&a[i][j+1]=='#'&&a[i][j+2]=='#'&&a[i][j+3]=='#')
				return 2;
		}
	}
	return 0;
}
int judge2()  //列  ,返回1表示player1 胜，返回2表示player2胜 
{
	for(int i=0;i<6;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(a[j][i]=='O'&&a[j][i+1]=='O'&&a[j][i+2]=='O'&&a[j][i+3]=='O')
				return 1;
			if(a[j][i]=='#'&&a[j][i+1]=='#'&&a[j][i+2]=='#'&&a[j][i+3]=='#')
				return 2;
		}
	}
	return 0;
}
int judge3()  //主对角 ,返回1表示player1 胜，返回2表示player2胜 
{
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(a[i][j]=='O'&&a[i+1][j+1]=='O'&&a[i+2][j+2]=='O'&&a[i+3][j+3]=='O')
				return 1;
			if(a[i][j]=='#'&&a[i+1][j+1]=='#'&&a[i+2][j+2]=='#'&&a[i+3][j+3]=='#')
				return 2;
		}
	}
	return 0;
}
int judge4()  //副对角 , 返回1表示player1 胜，返回2表示player2胜 
{
	for(int i=3;i<6;i++)
	{
		for(int j=2;j>=0;j--)
		{
			if(a[i][j]=='O'&&a[i-1][j+1]=='O'&&a[i-2][j+2]=='O'&&a[i-3][j+3]=='O')
				return 1;
			if(a[i][j]=='#'&&a[i-1][j+1]=='#'&&a[i-2][j+2]=='#'&&a[i-3][j+3]=='#')
				return 2;
		}
	}
	return 0;
}
int main()
{
	int a=judge1(),b=judge2(),c=judge3(),d=judge4();
	if(a==1)
		cout<<"winner: Player1"<<endl;
	else if(a==2)
		cout<<"winner: Player2"<<endl;
	else if(b==1)
		cout<<"winner: Player1"<<endl;
	else if(b==2)
		cout<<"winner: Player2"<<endl;
	else if(c==1)
		cout<<"winner: Player1"<<endl;
	else if(c==2)
		cout<<"winner: Player2"<<endl;
	else if(d==1)
		cout<<"winner: Player1"<<endl;
	else if(d==2)
		cout<<"winner: Player2"<<endl;
	else
		cout<<"no winner"<<endl;
	return 0;
}
