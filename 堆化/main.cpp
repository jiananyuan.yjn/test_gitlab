#include <iostream>
using namespace std;
int a[500001],n;
void maxHeapify(int i) {
    int l=2*i;
    int r=2*i+1;
    int largest=i;
    if(l<=n && a[l]>a[i])        largest=l;
    if(r<=n && a[r]>a[largest])  largest=r;
    if(largest!=i) {
        swap(a[i],a[largest]);
        maxHeapify(largest);
    }
}
int main()
{
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    for(int i=n/2;i>=1;i--)
        maxHeapify(i);
    for(int i=1;i<=n;i++)
        cout<<" "<<a[i];
    cout<<endl;
    return 0;
}
