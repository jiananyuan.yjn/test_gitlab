#include<iostream>
#include<cstdio>
using namespace std;
int main()
{
	unsigned long long x;
	cin>>x;
	int y=1970,m=1,d=1,h=0,f=0,s=0,temp=3600*24,total_day=x/temp,total_sec=x-total_day*3600*24;
	while ( total_day >=366 )
	{
		if( y%400==0 || (y%4==0 && y%100!=0) )
			total_day-=366;
			
		else
			total_day-=365;
		
		y++;
	}
	
	if( !(y%400==0 || (y%4==0 && y%100!=0)) && total_day==365 )
		total_day-=365;
	//compute the year successfully and 
	//calculate the day remaining in the year
	
	while(total_day--)
	{
		d++;
		if( (  d>31 && (m==1||m==3||m==5||m==7||m==8||m==10||m==12)  ) ||
			( (d>29 && m==2) && ( y%400==0 || (y%4==0 && y%100!=0))  ) ||
			(  d>30 && (m==4||m==6||m==9||m==11)                     ) ||
			( (d>28 && m==2) && !( y%400==0 || (y%4==0 && y%100!=0)) ) )
			{
				m++;
				d=1;
			}
	}
	//compute the month and date successfully
	
	while(total_sec--)
	{
		s++;
		if(s==60)
		{
			s=0;
			f++;
		}
		
		if(f==60)
		{
			f=0;
			h++;
		}
		
		if(h==24)
			h=0;
	}
	//calculate the hour,minute and second successfully
	
	if(h+8<24)
		h+=8;
	else
	{
		h=h+8-24;
		d++;
		if( (  d>31 && (m==1||m==3||m==5||m==7||m==8||m==10||m==12)  ) ||
			( (d>29 && m==2) && ( y%400==0 || (y%4==0 && y%100!=0))  ) ||
			(  d>30 && (m==4||m==6||m==9||m==11)                     ) ||
			( (d>28 && m==2) && !( y%400==0 || (y%4==0 && y%100!=0)) ) )
			{
				m++;
				d=1;
			}
			
		if(m>12)
		{
			y++;
			m=1;
		}
	}
	
	printf("%d/%02d/%02d %02d:%02d:%02d",y,m,d,h,f,s);
}
