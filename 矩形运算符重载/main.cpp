#include<iostream>
using namespace std;
class CPoint{
protected:
    int x,y;
public:
    CPoint(int _x,int _y):x(_x),y(_y){}
    CPoint(const CPoint& t)
    {
        x=t.getX();
        y=t.getY();
    }
    int getX()const {return x;}
    int getY()const {return y;}
    friend bool operator==(const CPoint& a,const CPoint& b);
};
bool operator==(const CPoint& a,const CPoint& b)
{
    if(a.getX()==b.getX()&&a.getY()==b.getY())
        return true;
    return false;
}
class CRectangle{
protected:
    CPoint leftPoint,rightPoint;
public:
    CRectangle(const CPoint& a,const CPoint& b):leftPoint(a),rightPoint(b){}
    bool operator>(const CPoint& p)const//若p在矩形内，返回true,否则返回false。
    {
        if(p.getX()>=leftPoint.getX()&&p.getX()<=rightPoint.getX())
            if(p.getY()>=rightPoint.getY()&&p.getY()<=leftPoint.getY())
                return true;
        return false;
    }
    bool operator>(const CRectangle& p)const//第一个矩形若包含第二个矩形（部分边界可以相等），返回true，否则返回false。
    {
        if(*this>p.leftPoint)
            if(*this>p.rightPoint)
                return true;
        return false;
    }
    bool operator==(const CRectangle& p)const//判断两个矩形是否一致，返回true或false。
    {
        if(leftPoint==p.leftPoint)
            if(rightPoint==p.rightPoint)
                return true;
        return false;
    }
    bool operator*(const CRectangle& p)const//判断两个矩形是否有重叠部分，返回true或false。
    {
        if(rightPoint.getX()<p.leftPoint.getX() || leftPoint.getX()>p.rightPoint.getX())
            return false;
        if(rightPoint.getY()>p.leftPoint.getY() || leftPoint.getY()<p.rightPoint.getY())
            return false;
        return true;
    }
    operator int()const//计算矩形的面积并返回，面积是整型。
    {
        int dx=rightPoint.getX()-leftPoint.getX();
        int dy=leftPoint.getY()-rightPoint.getY();
        return dx*dy;
    }
    friend ostream& operator<<(ostream& out,const CRectangle& t);
};
//输出矩形的两个角坐标和面积，具体格式见样例。
ostream& operator<<(ostream& out,const CRectangle& t)
{
    out<<t.leftPoint.getX()<<' '<<t.leftPoint.getY()<<' '<<t.rightPoint.getX()<<' '<<t.rightPoint.getY();
    return out;
}
int main()
{
    int t,x1,x2,y1,y2;
    cin>>t;
    while(t--)
    {
        cin>>x1>>y1>>x2>>y2;
        CPoint a(x1,y1);
        CPoint b(x2,y2);
        CRectangle rect1(a,b);
        cin>>x1>>y1>>x2>>y2;
        CPoint c(x1,y1);j
        CPoint d(x2,y2);
        CRectangle rect2(c,d);
        cout<<"矩形1:"<<rect1<<" "<<rect1<<endl;
        cout<<"矩形2:"<<rect2<<" "<<(int)rect2<<endl;
        if(rect1==rect2)
            cout<<"矩形1和矩形2相等"<<endl;
        else if(rect2>rect1)
            cout<<"矩形2包含矩形1"<<endl;
        else if(rect1>rect2)
            cout<<"矩形1包含矩形2"<<endl;
        else if(rect1*rect2)
            cout<<"矩形1和矩形2相交"<<endl;
        else
            cout<<"矩形1和矩形2不相交"<<endl;
        cout<<endl;
    }
    return 0;
}
