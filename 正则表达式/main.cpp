#include<iostream>
#include<stack>
#include<cstdlib>
#include<cmath>
#include<cctype>
#include<string>
using namespace std;
int main()
{
	string s;
	cin >> s;
	string temp;
	stack<int> v;
	for (int i = 0; i < s.length(); i++)
	{
	    if (isdigit(s[i]))
		{
			char w[2];
			w[0] = s[i];
			w[1] = '\0';
			temp += w;
		}
		if (s[i] == '@')
		{
			cout << v.top() << endl;
			v.pop();
			return 0;
		}
		if (s[i] == '.')
		{
			int a = atoi(temp.c_str());
			temp.clear();
			v.push(a);
		}
		if (s[i] == '+')
		{
			int a = v.top();
			v.pop();
			int b = v.top();
			v.pop();
			v.push(b + a);
		}
		if (s[i] == '-')
		{
			int a = v.top();
			v.pop();
			int b = v.top();
			v.pop();
			v.push(b - a);
		}
		if (s[i] == '*')
		{
			int a = v.top();
			v.pop();
			int b = v.top();
			v.pop();
			v.push(b * a);
		}
		if (s[i] == '/')
		{
			int a = v.top();
			v.pop();
			int b = v.top();
			v.pop();
			v.push(b / a);
		}
	}
}
