#include <iostream>
using namespace std;
int n;
int a[25];
int solve(int des,int index)
{
    if(index>n) return 0;
    if(des==0)  return 1;
    return solve(des-a[index],index+1)||solve(des,index+1);
}
int main()
{
    cin>>n;
    for(int i=0;i<n;i++)  cin>>a[i];
    int t;
    cin>>t;
    while(t--)
    {
        int x;
        cin>>x;
        int flag=solve(x,0);
        if(flag)  cout<<"yes"<<endl;
        else      cout<<"no"<<endl;
    }
    return 0;
}
