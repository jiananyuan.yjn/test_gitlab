#include <bits/stdc++.h>
#define LL long long
using namespace std;
const int next[][2]={ {1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-2,1},{-1,2} };
int dx,dy,rx,ry;
bool horse[25][25];
LL num[25][25];

int main()
{
    num[1][1]=1;
    cin>>dx>>dy>>rx>>ry;
    dx++;dy++;rx++;ry++;
    horse[rx][ry]=1;
    for(int i=0;i<8;i++) {
        int x=rx+next[i][0];
        int y=ry+next[i][1];
        if(x>=1&&x<=dx&&y>=1&&y<=dy)
            horse[x][y]=1;
    }
    for(int i=1;i<=dx;i++) {
        for(int j=1;j<=dy;j++) {
            if(horse[i][j])
                continue;
            num[i][j]=max(num[i][j],num[i-1][j]+num[i][j-1]);
        }
    }
    cout<<num[dx][dy]<<endl;
    return 0;
}
