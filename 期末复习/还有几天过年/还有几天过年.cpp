#include<bits/stdc++.h>
int Week(int m1,int d1,int m2,int d2,int flag,int w);
using namespace std;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int y,m1,d1,m2,d2,w;
		scanf("%d/%d/%d %d",&y,&m1,&d1,&w);
		scanf("%d/%d/%d",&y,&m2,&d2);
		
		int s,flag,r;
		if(y%400==0||(y%4==0&&y%100!=0))
			flag=1;
			
		else
			flag=0;
			
		s=Week(m1,d1,m2,d2,flag,w);
		
		printf("%d/%02d/%02d是星期",y,m2,d2);
		
		r=(s+w)%7;
		
		switch(r)
		{
			case 0:printf("日\n");break;
			case 1:printf("一\n");break;
			case 2:printf("二\n");break;
			case 3:printf("三\n");break;
			case 4:printf("四\n");break;
			case 5:printf("五\n");break;
			case 6:printf("六\n");break;
		}
	}
}

int Week(int m1,int d1,int m2,int d2,int flag,int w)
{
	int s=0;
	int run[12]={31,29,31,30,31,30,31,31,30,31,30,31};
	int ping[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	if(flag==1)
	{
		s+=run[m1-1]-d1;
		for(int i=m1;i<m2-1;i++)
			s+=run[i];
		s+=d2;	
	}
	
	if(flag==0)
	{
		s+=ping[m1-1]-d1;
		for(int i=m1;i<m2-1;i++)
			s+=ping[i];
		s+=d2;	
	}
	
	return s;
}
