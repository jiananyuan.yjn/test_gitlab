#include<iostream>
#include<cmath>
void itostr(int num,char str[]);
using namespace std;
int main()
{
    const int SIZE = 20;
    int t,num;
    char str[SIZE];

    cin>>t;
    while(t--)
    {
        cin>>num;
        itostr(num,str);
        cout<<str<<endl;
    }
    return 0;
}

void itostr(int num,char str[])//the structure of logic!
{
    int count=0,temp=num;

    if(num<0)
    {
        str[0]='-';
        count++;
    }

    if(num==0)
    {
        str[0]='0';
        str[1]='\0';
        return ;
    }

    itostr(num/10,str);

    for(  ;temp!=0;count++)
        temp/=10;

    str[count-1]=fabs(num%10)+'0';
    str[count]='\0';
}
