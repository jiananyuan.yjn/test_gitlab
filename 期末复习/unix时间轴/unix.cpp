#include<bits/stdc++.h>
void Unix(long s);
using namespace std;
int main()
{
	long s;
	cin>>s;
	Unix(s);	
} 

void Unix(long s)
{
	int year=1970,month=1,day=1,hour=8,minute=0,second=0;
	
	int dis_day=s/(24*3600),r_s=s%(3600*24),memory=0;
	
	for(int i=1;i<=dis_day;i++)
	{
		day++;
		
		if(month==1||month==3||month==5||month==7||month==8||month==10||month==12)
		{
			if(day>31)
			{
				day-=31;
				month++;
				
				if(month>12)
				{
					month-=12;
					year++;
				}
			}
		}
		
		if(month==2)
		{
			if(year%400==0||(year%4==0||year%100!=0))
				if(day>29)
				{
					day-=29;
					month++;
					
					if(month>12)
					{
						month-=12;
						year++;
					}
				}
				
			else
				if(day>28)
				{
					day-=28;
					month++;
					
					if(month>12)
					{
						month-=12;
						year++;
					}
				}
		}
		
		if(month==4||month==6||month==9||month==11)
		{
			if(day>30)
			{
				day-=30;
				month++;
				
				if(month>12)
				{
					month-=12;
					year++;
				}
			}
		}
	}
	
	printf("%d/%d/%d %d:%d:%d",year,month,day,hour,minute,second);
}
