#include <iostream>
#include<stdio.h>
#include<cmath>
using namespace std;
const double pie=4*atan(1.0);
const double eps = 1e-6;
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int n,f;
        cin>>n>>f;
        f++;
        double s[n];
        double maxn=-1e9;
        for(int i=0;i<n;i++)
        {
            int r;
            cin>>r;
            maxn=maxn>r?maxn:r;
            s[i]=pie*r*r;
        }
        double m,l=0,r=pie*maxn*maxn;
        while(r-l>eps)
        {
            m=(l+r)/2;
            int sum=0;
            for(int i=0;i<n;i++)
                sum+=int(s[i]/m);
            if(sum<f)  //分得太大了
                r=m;
            else       //sum>f 还可以继续二分使得每份变得更大
                l=m;
        }
        printf("%.4lf\n",m);
    }
    return 0;
}
