#include <stdio.h>
#include <vector>
#define work(opt) nxt.clear();\
            nxt.push_back(a opt b);\
            for(int k = 0; k < v.size(); k ++)\
                if(k != i && k != j) nxt.push_back(v[k]);\
            if(dfs(k + 1, nxt)) {\
                sprintf(ans[pt ++], "%d"#opt"%d=%d\n", max(a, b), min(a, b), a opt b);\
                return true ;\
            }
using namespace std;
int a[4], pt;
char ans[5][101];

bool dfs(int k, const vector<int> & v) {
    if(k == 3) return v.size() == 1 && v[0] == 24; //成功
    if(v.size() <= 1) return false ;               //如果经过1~2步就得到24则失败
    vector<int> nxt;                               //下一个状态
    for(int i = 0; i < v.size(); i ++) {           //枚举第一个操作数
        for(int j = 0; j < v.size(); j ++) {      //枚举第二个操作数
            if(i == j) continue ;
            int a = v[i], b = v[j];
            work(+);
            if(a > b) {work(-)}
            work(*);
            if(a % b == 0) {work(/)}
        }
    }
    return false ;
}

int main() {
    scanf("%d%d%d%d", &a[0], &a[1], &a[2], &a[3]);
    vector<int> s;
    for(int i = 0; i < 4; i ++)
        s.push_back(a[i]);
    if(!dfs(0, s)) puts("No answer!");
    else for(int i = pt - 1; ~ i; i --)
            printf(ans[i]);
    return 0;
}
