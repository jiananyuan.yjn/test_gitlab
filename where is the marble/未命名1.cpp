#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
int main()
{
	int n,m,Case=1;
	while(cin>>n>>m)
	{
		if(n==0 && m==0)
			return 0;
		cout<<"CASE# "<<Case++<<':'<<endl;
		vector<int>marble;
		for(int i=0;i<n;i++)
		{
			int t;
			cin>>t;
			marble.push_back(t);
		}
		
		sort(marble.begin(),marble.end());
		
		for(int i=0;i<m;i++)
		{
			int t;
			cin>>t;
			int p=lower_bound(marble.begin(),marble.end(),t)-marble.begin();
			if(marble[p]==t)
				cout<<t<<" found at "<<p+1<<endl;
			else
				cout<<t<<" not found"<<endl;
		}
	}
}
