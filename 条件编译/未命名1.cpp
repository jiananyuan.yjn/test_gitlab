#include<iostream>
using namespace std;
#define INTEGER
#ifdef INTEGER
	int add(int x,int y)
	{
		return (x+y);
	}
#else
	float add(float x,float y)
	{
		return (x+y);
	}
#endif

int main()
{
	#ifdef INTEGER
		int a,b,c;
		cin>>a>>b;
		cout<<add(a,b)<<endl;
		
	#else
		float a,b,c;
		cin>>a>>b;
		cout<<add(a,b)<<endl;
	#endif
} 
