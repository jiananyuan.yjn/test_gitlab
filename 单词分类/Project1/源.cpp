#include<iostream>
#include<cstring>
#include<cstdlib>
using namespace std;

class word
{
private:
	char str[102];
public:
	int cnt[26] = { 0 }, len = strlen(str);
	void set(char string[]);
	void num_of_alpha();
	void change_status();
	int status = 1;
};

int count(word *p,int n);

int main()
{
	int n;
	cin >> n;
	word *p = new word[n];
	for (int i = 0; i < n; i++)
	{
		char string[102];
		cin >> string;
		p[i].set(string);
		p[i].num_of_alpha();
	}
	int t=count(p,n);
	cout << t << endl;

	delete[]p;
	system("pause");
	return 0;
}

void word::set(char string[])
{
	strcpy_s(str, string);
}

void word::num_of_alpha()
{
	for (int i = 0; i < len; i++)
	{
		int dis = str[i] - 'A';
		cnt[dis]++;
	}
}

void word::change_status()
{
	status = 0;
}

int count(word *p,int n)
{
	int cnt = 0;
	for (int i = 0; i < n-1; i++)
	{
		loop1:
		for (int j = i+1; j < n && p[j].status ; j++)
		{
			for (int k = 0; k < 26; k++)
			{
				if (p[i].cnt[k] != p[j].cnt[k])
					goto loop1;
			}
			p[j].change_status();
			cnt++;
		}
	}
	return cnt;
}