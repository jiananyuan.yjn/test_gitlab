#include<iostream>
using namespace std;
typedef long long LL;
const int mod=10056;
#define MAX 1005
LL ans[MAX];
LL dp[MAX][MAX];
void init()
{

    dp[1][1]=1;
    ans[1]=dp[1][1];
    for(int i=2;i<=1000;i++)
    {
        for(int j=1;j<=i;j++)
        {
            dp[i][j]=(dp[i-1][j-1]*j%mod+dp[i-1][j]*j%mod)%mod;
            ans[i]=(ans[i]+dp[i][j])%mod;
        }
    }
}
int main()
{
    init();
    int t;
    cin>>t;
    for(int k=1;k<=t;k++)
    {
        int n;
        cin>>n;
        cout<<"Case "<<k<<": ";
        cout<<ans[n]<<endl;
    }
    return 0;
}
