#include <iostream>
#include <stdlib.h>
#include <vector>
#define UP 1
#define RIGHT 2
#define DOWN 3
#define LEFT 4
#define MAX 15
using namespace std;
struct point {
    int x,y;
    point(int a=0,int b=0):x(a),y(b){}
};
int n,m;
int mp[MAX][MAX];
bool book[MAX][MAX];
vector<point> v;
int dir[][2] = { {-1,0},{0,1},{1,0},{0,-1} };
inline bool check(int x,int y) {
    if(x<1 || y<1 || x>n || y>m || mp[x][y]==0 || book[x][y]==1) return false;
    return true;
}

void dfs(int x,int y,int cur) {
    if(x == n && y== m+1) {
        cout<<"("<<v[0].x<<","<<v[0].y<<")";
        for(int i=1;i<v.size();i++) {
            cout<<" ("<<v[i].x<<","<<v[i].y<<")";
        }
        exit(0);
    }
    if(!check(x,y)) return ;
    book[x][y] = 1; v.push_back(point(x,y));
    if(mp[x][y] == 5 || mp[x][y] == 6) {
        if(cur==RIGHT) dfs(x,y-1,RIGHT);
        if(cur==LEFT)  dfs(x,y+1,LEFT);
        if(cur==UP)    dfs(x+1,y,UP);
        if(cur==DOWN)  dfs(x-1,y,DOWN);
    }
    else {
        if(cur==RIGHT) dfs(x-1,y,DOWN),dfs(x+1,y,UP);
        if(cur==LEFT)  dfs(x-1,y,DOWN),dfs(x+1,y,UP);
        if(cur==UP)    dfs(x,y-1,RIGHT),dfs(x,y+1,LEFT);
        if(cur==DOWN)  dfs(x,y-1,RIGHT),dfs(x,y+1,LEFT);
    }
    book[x][y] = 0; v.pop_back();
}
int main() {
    cin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            cin>>mp[i][j];
    dfs(1,1,LEFT); //�ӿ������
    cout<<"impossible"<<endl;
    return 0;
}
