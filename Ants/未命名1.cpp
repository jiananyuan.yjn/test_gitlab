/*#include<iostream>
#include<algorithm>
const int MAX=101;
int map[MAX][MAX];
int dp[MAX][MAX];
int dpx[]={-1,0,1,0};
int dpy[]={0,-1,0,1};
int n,m;
using namespace std;
int search_max(int x,int y){
	int i;
	int vx,vy;
	if(dp[x][y])
		return dp[x][y];
		
	for(int i=0;i<4;i++){
		vx=x+dpx[i];
		vy=y+dpy[i];
		if(map[vx][vy]!=-1&&vx>=0&&vx<n&&vy>=0&&vx<m&&map[vx][vy]<map[x][y])
			dp[x][y]=max(dp[x][y],search_max(vx,vy)+1);
	}
	return dp[x][y];
}
int main(){
	int i,j;
	int maxn=0;
	cin>>n>>m;
		for(i=0;i<MAX;i++){
			for(j=0;j<MAX;j++){
				map[i][j]=-1;
			}
		}
		
		for(i=1;i<=n;i++){
			for(j=1;j<=m;j++){
				cin>>map[i][j];
				dp[i][j]=0;
			}
		}
		
		for(i=1;i<=n;i++){
			for(j=1;j<=m;j++){
				maxn=max(maxn,search_max(i,j));
			}
		}
		cout<<maxn+1<<endl;
	return 0;
}
*/
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;
const int maxn = 105;
int n,m;
int dp[maxn][maxn];
int maze[maxn][maxn];
int dir[4][2] = {{1,0},{-1,0},{0,-1},{0,1}}; 
bool judge(int x,int y)   //判断是否越界
{    
	if(x < 1 || x > n || y < 1 || y > m)        
		return false;    
	return true;
} 
int dfs(int i,int j) {    
	if(dp[i][j])         //如果已经搜索过，直接返回该值就行了        
		return dp[i][j];    
	int ret = 1;    
	for(int k = 0; k < 4; k++){        
		int xx = i + dir[k][0];        
		int yy = j + dir[k][1];       
		if(judge(xx,yy) && maze[i][j] > maze[xx][yy])  //同时要保证比旁边的数大才能继续搜索            
		ret = max(dfs(xx,yy)+1, ret);       
	}    
	dp[i][j] = ret;    //如果没法搜索说明这个点就是最低点    
	return ret;
} 
int main(){    
	while(scanf("%d %d", &n, &m) != EOF){        
		memset(dp,0,sizeof(dp));        
		memset(maze,0,sizeof(maze));        
		for(int i = 1; i <= n; i++)            
			for(int j = 1; j <= m; j++)                
				scanf("%d", &maze[i][j]);        
		int ans = 0;        
		for(int i = 1;  i <= n; i++)            
			for(int j = 1; j <= m; j++)                
				ans = max(dfs(i,j), ans);        
		printf("%d\n", ans);    
		}    
	return 0;
}
