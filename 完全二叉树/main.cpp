#include <iostream>
using namespace std;
int a[10000];
int parent(int i) {return i/2;}
int left(int i) {return 2*i;}
int right(int i) {return 2*i+1;}
int main()
{
    int n;
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    for(int i=1;i<=n;i++) {
        cout<<"node "<<i<<": key = "<<a[i]<<", ";
        if(parent(i)>=1)
            cout<<"parent key = "<<a[parent(i)]<<", ";
        if(left(i)<=n)
            cout<<"left key = "<<a[left(i)]<<", ";
        if(right(i)<=n)
            cout<<"right key = "<<a[right(i)]<<", ";
        cout<<endl;
    }
    return 0;
}
