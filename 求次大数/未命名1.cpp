#include<iostream>
using namespace std;
int main()
{
	int x,maxn=-1,ans;
	int n;
	cin>>n;
	while(n--)
	{
		cin>>x;
		if(x>maxn)
		{
			ans=maxn;
			maxn=x;
		}
		else
		{
			if(x>ans)
				ans=x;
		}
	}
	cout<<ans<<endl;
	return 0;
}
