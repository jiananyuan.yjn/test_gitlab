#include <iostream>
#include<string>
using namespace std;
int a[26];
int main()
{
    int maxn=-1e9;
    for(int u=0;u<4;u++)
    {
        string s;
        getline(cin,s);
        for(int i=0;i<s.length();i++)
        {
            if(isalpha(s[i]))
            {
                a[ s[i]-'A' ] ++;
                maxn=maxn>a[ s[i]-'A' ]?maxn:a[ s[i]-'A' ];
            }
        }
    }
    for(int k=maxn;k>=1;k--)
    {
        for(int i=0;i<26;i++)
        {
            if(a[i]>=k)
                cout<<"* ";
            else
                cout<<"  ";
        }
        cout<<endl;
    }
    char ch='A';
    cout<<ch;
    for(ch='B';ch<='Z';ch++)
        cout<<" "<<ch;
    return 0;
}
