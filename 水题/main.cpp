#include<bits/stdc++.h>
using namespace std;
int main()
{
    int n,k,i,count=0,cnt=0,temp,book[10000]={0};
    cin>>n>>k;

    if(k>n)
    {
        cout<<"NO RESULT"<<endl;
        exit(-1);
    }

    for(i=0;i<n;i++)
    {
        cin>>temp;
        book[temp]++;
    }

    for(i=0;i<n;i++)
    {
        if(book[i]!=0)
            count++;
    }

    if(count<k)
    {
        cout<<"NO RESULT"<<endl;
        exit(-1);
    }

    for(i=0;i<n;i++)
    {
        if(book[i]!=0)
            cnt++;

        if(cnt==k)
        {
            cout<<i<<endl;
            break;
        }
    }
}
