#include<iostream>
using namespace std;
int main()
{
	int n;
	cin>>n;
	int x[705],y[705],i=0;
	for(int j=0;j<n;j++)
		cin>>x[j]>>y[j];
	int maxx=x[0],maxy=y[0];
	for(int j=1;j<n;j++)
	{
		maxx= maxx > x[j] ? maxx : x[j];
		maxy= maxy > y[j] ? maxy : y[j];
	}
	
	int **p=new int *[maxx];
	for(int j=0;j<maxx;j++)
		p[j]=new int[maxy];
		
	for(i=0;i<maxx;i++)
		for(int j=0;j<maxy;j++)
			p[i][j]=0;
			
	for(int j=0;j<n;j++)
		p[ x[j]-1 ][ y[j]-1 ]=1;
		
	int maxn=-99999999,s=0;
	
	for(i=0;i<maxx;i++)
	{
		s=0;
		for(int j=0;j<maxy;j++)
			if(p[i][j])
				s++;
		maxn=maxn>s?maxn:s;
	}
	
	for(i=0;i<maxy;i++)
	{
		s=0;
		for(int j=0;j<maxx;j++)
			if(p[j][i])
				s++;
		maxn=maxn>s?maxn:s;
	} 
	
	for(i=0;i<maxx;i++)
	{
		for(int j=maxy-1;j>=0;j--)
		{
			int k1=i,k2=j,s=0;
			while(k1<maxx && k2<maxy)
				s+=p[k1++][k2++];
			maxn=maxn>s?maxn:s;
		}
	}
	
	cout<<maxn<<endl;
	for(int j=0;j<maxx;j++)
		delete[]p[j];
	delete[]p;
} 
