#include <iostream>
#include<algorithm>
using namespace std;
int a[2000001],n=0;
#define inf (1<<30)
void maxHeapify(int i) {
    int l=2*i;
    int r=2*i+1;
    int largest=i;
    if(l<=n && a[l]>a[i])        largest=l;
    if(r<=n && a[r]>a[largest])  largest=r;
    if(largest!=i) {
        swap(a[i],a[largest]);
        maxHeapify(largest);
    }
}
int extract() {
    int maxv;
    if(n<1) return -inf;
    maxv=a[1];
    a[1]=a[n--];
    maxHeapify(1);
    return maxv;
}
void increasekey(int i,int x) {
    if(x<a[i])    return ;
    a[i]=x;
    while(i>1 && a[i/2] < a[i] ) {
        swap(a[i],a[i/2]);
        i/=2;
    }
}
void insert(int x) {
    n++;
    a[n]=-inf;
    increasekey(n,x);
}
int main()
{
    string op;
    while(cin>>op) {
        if(op=="end")   break;
        if(op=="insert") {
            int x;
            cin>>x;
            insert(x);
        }
        if(op=="extract") {
            cout<<extract()<<endl;
        }
    }
    return 0;
}
